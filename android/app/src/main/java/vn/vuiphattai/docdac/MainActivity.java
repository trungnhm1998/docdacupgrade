package vn.vuiphattai.docdac;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.reactnativenavigation.NavigationActivity;

public class MainActivity extends NavigationActivity {
    // https://github.com/wix/react-native-navigation/issues/3389
    // for setting splash screen using v2

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
    }

}
