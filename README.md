## How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the following script

* Install React & React Native
```
npm i -S react-native@0.43.3
npm i -S react@16.0.0-alpha.6
```

* Install scripts module
```
npm install
npm link
```
For most of the dependecies on android we have to manually check and link it

* Install ESlint and Debug
```
npm i --save-dev babel-eslint eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-react eslint-plugin-jsx-a11y remote-redux-devtools
```

* Install Encrypt Tool
```
npm i -S react-native-encrypt-tool 
```

* Install Navigation (if needed)
```
npm install -S react-native-navigation@latest
```

## Run iOS

Require Xcode 8 or higher

```
react-native run-ios
```

Build and test on Device

```
$ npm run ios:device
```

## Linting

We are using ESLINT Airbnb. You can make sure this module lints with itself using

## Run iOS

* Development
```
$ npm run script ios:build
```

* Production
```
$ npm run script ios:release
```


## Run Android
* Development
```
$ npm run script android:build
```

* Production
```
$ npm run script android:release
```

* Signed APK
```
$ npm run script android:signedAPK
while building android for release sometimes the bundle couldn't update so we have to delete
index.android.* in side android/app/src/main/assets
then run
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
to generate new bundle for android
src:[https://stackoverflow.com/questions/44446523/unable-to-load-script-from-assets-index-android-bundle-on-windows]
```
