import { Dimensions, StyleSheet, PixelRatio } from 'react-native'

const { height, width } = Dimensions.get('window')
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 10 / PixelRatio.get(),
    padding: 14
  },
  row: {
    flexDirection: 'row',
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 4
  },
  circleLine: {
    width: 8,
    height: 8,
    borderRadius: 4,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#c92033'
  },
  contentContainer: {
    padding: 10
  },
  boardNum: {
    width: width * 0.09,
    height: width * 0.09,
    borderWidth: 3,
    marginTop: 10,
    borderColor: '#7E0590',
    borderRadius: (width * 0.09) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  numberCover: {
    flex: 2,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  },
  rowCover: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 10
  },
  numberStyle: {
    color: '#c92033'
  },
  borderBottom: {
    borderBottomWidth: 0.5 * PixelRatio.get(),
    borderBottomColor: '#f2f2f2',
    marginTop: 10
  },
  roundButton: {
    height: 40,
    borderRadius: 20,
    padding: 0,
    justifyContent: 'center',
    borderWidth: 0.5 * PixelRatio.get(),
    borderColor: '#c92033'
  }
})
