import { Dimensions, StyleSheet } from 'react-native'

const { height, width } = Dimensions.get('window')
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  mainContainer: {
    flex: 1
  },
  scrollViewContainer: {
    paddingLeft: 10,
    paddingRight: 10
  },
  boardNum: {
    width: width * 0.09,
    height: width * 0.09,
    borderWidth: 3,
    borderColor: '#f15a22',
    borderRadius: (width * 0.09) / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  numberStyle: {
    color: '#06b606'
  },
  randomBoard: {
    width: width * 0.6,
    borderRadius: 4,
    borderColor: '#c92033',
    borderWidth: 1,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textButtton: {
    color: '#c92033',
    paddingHorizontal: 15,
    paddingVertical: 6
  },
  buttonGroup: {
    alignItems: 'center',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  ratioItem: {
    width: 20,
    height: 20,
    borderColor: '#c92033',
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  selectRatio: {
    width: 8,
    height: 8,
    borderRadius: 4
  },
  selectedRatio: {
    backgroundColor: '#c92033'
  },
  ratioItemList: {
    flexDirection: 'row',
    marginTop: 10
  },
  ratioTextLbabel: {
    color: '#222222'
  },
  selectLoation: {
    flexDirection: 'row',
    marginTop: 10
  },
  picknumberModal: {
    width,
    height,
    backgroundColor: 'rgba(0,0,0,.8)'
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 1
  },
  modalPickup: {
    backgroundColor: 'transparent'
  },
  pickPlayMethod: {
    width: width * 0.55,
    paddingLeft: 10,
    paddingTop: 6,
    paddingBottom: 6
  },
  slideNumberPicker: {
    backgroundColor: '#c92033',
    borderRadius: 5,
    height: height - 80,
    padding: 10,
    marginHorizontal: 3
  },
  headerNumberPicker: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingBottom: 10
  },
  boardPos: {
    color: '#fff'
  },
  buttonNumberPicker: {
    padding: 5,
    backgroundColor: '#f8ae40',
    height: 30,
    justifyContent: 'center',
    flexDirection: 'row',
    marginLeft: 5,
    marginRight: 5,
    paddingLeft: width * 0.03,
    paddingRight: width * 0.03,
    borderRadius: 4
  },
  roundNumberPicker: {
    backgroundColor: '#d44d5c',
    flex: 1,
    borderRadius: 5,
    paddingTop: 10,
    paddingLeft: width * 0.01,
    paddingRight: width * 0.01
  },
  numberPicker: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  pickNumber: {
    backgroundColor: '#fff',
    width: width * 0.088,
    height: width * 0.088,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: width * 0.015,
    marginBottom: width * 0.015,
    marginLeft: width * 0.015,
    marginRight: width * 0.015
  },
  pickNumberSymbol: {
    backgroundColor: '#fff',
    width: width * 0.135,
    height: width * 0.14,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: width * 0.015,
    marginBottom: width * 0.015,
    marginLeft: width * 0.015,
    marginRight: width * 0.015
  },
  showSymbol: {
    height: 40,
    backgroundColor: '#960717',
    marginLeft: -3,
    marginRight: -3,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    flexDirection: 'row'
  },
  checkButton: {
    width: 20,
    height: 20,
    margin: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkicon: {
    fontSize: 18,
    color: '#D44D5C'
  },
  showSymbolLabel: {
    fontSize: 14,
    color: '#fff'
  },
  symbol: {
    width: width * 0.07,
    height: width * 0.07,
    resizeMode: 'contain'
  },
  pickerCount: {
    fontSize: 13,
    color: '#fff',
    paddingLeft: width * 0.02,
    marginBottom: 5
  }
})
