import { StyleSheet, PixelRatio } from 'react-native'

const styles = StyleSheet.create({
  bannerImg: {
    marginHorizontal: -20
  },
  rowSetting: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: '#e0e4e7',
    paddingVertical: 7
  },
  lastRowSetting: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 7
  },
  iconBox: {
    width: 50,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtTitle: {
    flex: 1,
    paddingRight: 10
  }
})

export default styles
