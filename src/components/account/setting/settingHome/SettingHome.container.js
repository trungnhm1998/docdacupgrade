import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Navigation as Navigator } from 'react-native-navigation'
import SettingHome from './SettingHome'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import {
  Themes
} from '../../../../ui'
import { Navigation } from '../../../../screens/navigation'
import * as AccountAction from '../../../../redux/account/account.actions'
class SettingHomeContainer extends Component {

  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)
    this.onPressIntroduce = this.onPressIntroduce.bind(this)
    this.onPressCompany = this.onPressCompany.bind(this)
    this.onPressProcedure = this.onPressProcedure.bind(this)
    this.onPressInformation = this.onPressInformation.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressWithdraw = this.onPressWithdraw.bind(this)
    this.navToLogin = this.navToLogin.bind(this)
  }

  onPressIntroduce() {
    Navigator.push(this.props.componentId, {
      component: {
        name: Config.screen.settingInfo.name,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            noBorder: true,
            background: {
              color: Themes.Colors.navColor
            },
            title: {
              color: '#fff',
              text: 'Giới thiệu cách chơi'
            },
            backButton: {
              title: '',
              color:'#fff'
            }
          },
          layout: {
            backgroundColor: 'transparent'
          },
          animations: {
            push: {
              waitForRender: true
            },
            showModal: {
              waitForRender: true
            }
          }
        },
        passProps: { uri: 'https://docdac.vn/gioi-thieu.html' }
      }
    })
  }

  onPressProcedure() {
    Navigator.push(this.props.componentId, {
      component: {
        name: Config.screen.settingInfo.name,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            noBorder: true,
            background: {
              color: Themes.Colors.navColor
            },
            title: {
              color: '#fff',
              text: 'Quy trình trả thưởng'
            },
            backButton: {
              title: '',
              color:'#fff'
            }
          },
          layout: {
            backgroundColor: 'transparent'
          },
          animations: {
            push: {
              waitForRender: true
            },
            showModal: {
              waitForRender: true
            }
          }
        },
        passProps: { uri: 'https://docdac.vn/quy-trinh-tra-thuong.html' }
      }
    })
  }

  onPressCompany() {
    Navigator.push(this.props.componentId, {
      component: {
        name: Config.screen.settingInfo.name,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            noBorder: true,
            background: {
              color: Themes.Colors.navColor
            },
            title: {
              color: '#fff',
              text: 'Đơn vị chủ quản'
            },
            backButton: {
              title: '',
              color:'#fff'
            }
          },
          layout: {
            backgroundColor: 'transparent'
          },
          animations: {
            push: {
              waitForRender: true
            },
            showModal: {
              waitForRender: true
            }
          }
        },
        passProps: { uri: 'https://docdac.vn/don-vi-chu-quan.html' }
      }
    })
  }

  onPressInformation() {
    Navigator.push(this.props.componentId, {
      component: {
        name: Config.screen.settingInfo.name,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            noBorder: true,
            background: {
              color: Themes.Colors.navColor
            },
            title: {
              color: '#fff',
              text: 'Thông tin liên hệ'
            },
            backButton: {
              title: '',
              color:'#fff'
            }
          },
          layout: {
            backgroundColor: 'transparent'
          },
          animations: {
            push: {
              waitForRender: true
            },
            showModal: {
              waitForRender: true
            }
          }
        },
        passProps: { uri: 'https://docdac.vn/thong-tin-lien-he.html' }
      }
    })

  }

  onPressWithdraw() {
    Navigator.push(this.props.componentId, {
      component: {
        // ...Config.screen.settingInfo,
        // title: 'Quy định rút tiền',
        // passProps: { uri: 'https://docdac.vn/quy-dinh-rut-tien.html' }
        name: Config.screen.settingInfo.name,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            noBorder: true,
            background: {
              color: Themes.Colors.navColor
            },
            title: {
              color: '#fff',
              text: 'Quy định rút tiền'
            },
            backButton: {
              title: '',
              color:'#fff'
            }
          },
          layout: {
            backgroundColor: 'transparent'
          },
          animations: {
            push: {
              waitForRender: true
            },
            showModal: {
              waitForRender: true
            }
          }
        },
        passProps: { uri: 'https://docdac.vn/quy-dinh-rut-tien.html' }
      } 
    })
  }

  navToLogin() {
    Navigator.popToRoot(this.props.componentId)
    Navigation.navToRoot()
    
  }

  onPressSubmit() {
    const { accountAction } = this.props
    accountAction.logout({
      nextScreen: this.navToLogin
    })
  }

  render() {
    return (
      <SettingHome
        onPressIntroduce={this.onPressIntroduce}
        onPressCompany={this.onPressCompany}
        onPressProcedure={this.onPressProcedure}
        onPressInformation={this.onPressInformation}
        onPressSubmit={this.onPressSubmit}
        onPressWithdraw={this.onPressWithdraw}
        navigator={Navigation}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingHomeContainer)
