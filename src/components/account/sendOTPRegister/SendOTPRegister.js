import React, { Component } from 'react'
import {
  View,
  Image,
  ImageBackground,
  Keyboard,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  PixelRatio 
} from 'react-native'
import Modal from 'react-native-modalbox'
import _ from 'lodash'
import { store } from '../../../configs/store.config'

import {
  Themes, TextFont, TcombForm, Button 
} from '../../../ui'
import styles from './sendOTPRegister.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  country: t.String,
  phone: t.String,
  refCode: t.String
})

class SendOTPRegister extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: {
        country: 'Việt Nam (+84)',
        countryCode: '84',
        phone: '',
        refCode: ''
      }
    }
    this.onChange = this.onChange.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.renderModalCountry = this.renderModalCountry.bind(this)
    this.setCountry = this.setCountry.bind(this)
    this.onFocusCountry = this.onFocusCountry.bind(this)
  }

  componentDidMount() {
    const { accountInfo } = store.getState()

    if (accountInfo.refCode !== '') {
      const currentState = this.state
      this.setState({
        value: {
          ...currentState,
          refCode: accountInfo.refCode
        }
      })
    }
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    const { phone, countryCode, refCode } = this.state.value
    if (phone !== '' && countryCode !== '') {
      this.props.onPressSubmit(phone, countryCode, refCode)
    }
  }

  onPressForgotPassword = () => {
    this.props.onPressForgotPassword()
  }

  onFocusCountry = () => {
    this.refs.country.open()
    Keyboard.dismiss()
  }

  setCountry = (countryCode, country) => {
    this.setState({
      value: {
        ...this.state.value,
        countryCode,
        country
      }
    })

    this.refs.country.close()
  }

  renderModalCountry() {
    const { countryInfos } = this.props

    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position="center" ref="country">
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(countryInfos, (data, index) => {
              const textValue = `${data.name} (+${data.id})`
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setCountry(data.id, textValue)} style={[styles.btnTouchModal]}>
                  <TextFont color={data.id === this.state.value.countryCode ? Themes.Colors.navColor : null}>{textValue}</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10 }}>
          {locals.inputs.country}
          {locals.inputs.phone}
          {locals.inputs.refCode}
        </View>
      )
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        country: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn quốc gia',
          onFocus: () => this.onFocusCountry(),
          placeholderTextColor: '#E7E7E7',
          config: {
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            },
            label: 'Nhập số điện thoại bạn đang sử dụng'
          },
          value: this.state.value.country,
          initState: true
        },
        phone: {
          template: TcombForm.InputFlagLogin,
          placeholder: '0904143147',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad'
        },
        refCode: {
          template: TcombForm.InputFlagLogin,
          placeholderTextColor: '#E7E7E7',
          config: {
            labelStyle: {
              marginLeft: 0,
              color: '#222424',
              textAlign: 'left'
            },
            containerStyle: {
              marginTop: 5
            },
            label: 'Nhập mã giới thiệu (không bắt buộc)'
          }
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        onChange={this.onChange}
        value={this.state.value}
        type={userAccount}
        options={options}
      />
    )
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <ImageBackground source={Themes.Images.loginBg} style={styles.loginBg} resizeMode="cover">
          <View style={{ flex: 1, paddingTop: 40 * PixelRatio.get() }}>
            <View style={[styles.loginContentTop]}>
              {this.renderForm()}
            </View>
            <View style={{ flex: 1 }}>
              <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
            </View>
          </View>
          {this.renderModalCountry()}
        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }
}
SendOTPRegister.propTypes = {}
export default SendOTPRegister
