import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'

import SendOTPRegister from './SendOTPRegister'
import * as AccountAction from '../../../redux/account/account.actions'
import Config from '../../../screens/config'

import { AccountKitHandler } from '../../../handler'
import { helper } from '../../../utils'

class SendOTPRegisterContainer extends Component {
  // static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE
  static options(passProps) {
    return Config.navigatorStyle.DEFAULT_NAV_STYLE
  }

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.navToRegister = this.navToRegister.bind(this)
  }

  navToLogin(phone, countryCode) {
    Navigation.push(this.props.componentId, {
      component: {
        ...Config.screen.login,
        passProps: { phone, countryCode }
      }
    })
  }

  navToRegister(phone, countryCode) {
    Navigation.push(this.props.componentId, {
      component: {
        ...Config.screen.register,
        passProps: { phone, countryCode }
      }
    })
  }

  navToSendActiveCode(phone, countryCode, countries) {
    AccountKitHandler.verifyPhone(phone, countryCode, countries).then((verifyData) => {
      const { token } = verifyData
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          ...Config.screen.registerAccountKit,
          passProps: { phone, countryCode, token }
        }
      })
    }).catch((error) => {
      if (__DEV__) {
        //console.log('SendOTPRegister -> navToSendActiveCode: ', error)
      }
    })
  }

  onPressSubmit(phone, countryCode, refCode) {
    const { accountAction } = this.props
    const { useAccountKit, countries } = this.props.payload.initConfig

    accountAction.verifyPhone(phone, countryCode, useAccountKit, refCode, {
      loginScreen: this.navToLogin.bind(this, phone, countryCode),
      registerScreen: this.navToRegister.bind(this, phone, countryCode),
      sendActiveCodeScreen: this.navToSendActiveCode.bind(this, phone, countryCode, countries)
    })
  }

  onPressForgotPassword() {
    Navigation.push(this.props.componentId, {
      component: { ...Config.screen.forgotPassword }
    })
  }

  render() {
    const { countryInfos } = this.props.payload.initConfig
    return (
      <SendOTPRegister
        onPressSubmit={this.onPressSubmit}
        onPressForgotPassword={this.onPressForgotPassword}
        navigator={Navigation}
        countryInfos={countryInfos}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendOTPRegisterContainer)
