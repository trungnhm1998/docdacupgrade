import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Navigation as RNN } from 'react-native-navigation'
import Login from './Login'
import Config from '../../../screens/config'
import { Navigation } from '../../../screens/navigation'
import * as AccountAction from '../../../redux/account/account.actions'
import { AccountKitHandler } from '../../../handler'
import { helper } from '../../../utils'

// const Fabric = require('react-native-fabric')

// const { Answers } = Fabric

class LoginContainer extends Component {
  // static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE
  static options(passProps) {
    const result = helper.combineRNNV2Style(Config.navigatorStyle.DEFAULT_NAV_STYLE, {
      topBar: {
        visible: true,
        drawBehind: false
      }
    })
    return result
  }

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onPressForgotPassword = this.onPressForgotPassword.bind(this)
    this.navToForgotPassword = this.navToForgotPassword.bind(this)
    this.state = {
      showLoading: false
    }
  }

  navToForgotPassword(phone) {
    RNN.push(this.props.componentId, {
      component: {
        ...Config.screen.forgotPassword,
        passprops: { phone }
      }
    })
  }

  onPressSubmit(pass) {
    const { accountAction, payload } = this.props
    const { phone, countryCode } = payload.accountInfo
    this.setState({ showLoading: true })
    accountAction.authorize(phone, countryCode, pass, {
      nextScreen: () => Navigation.navToHome()
    })
  }

  static getDerivedStateFromProps(props, state) {
    if (props.payload.apiResponse.message) {
      return {
        showLoading: false
      }
    }
    return null
  }

  onPressForgotPassword() {
    const { accountAction, payload } = this.props
    const { phone, countryCode } = payload.accountInfo
    const { useAccountKit, countries } = payload.initConfig

    if (useAccountKit) {
      AccountKitHandler.verifyPhone(phone, countryCode, countries)
        .then((verifyData) => {
          const { token } = verifyData
          RNN.setStackRoot(this.props.componentId, {
            component: {
              ...Config.screen.forgotPasswordAccountKit,
              passProps: { phone, countryCode, token }
            }
          })
        }).catch((error) => {
          if (__DEV__) {
            //console.log('forgot password fail', error)
          }
        })
    } else {
      accountAction.sendOTPForgotPassword(phone, {
        forgotPasswordScreen: this.navToForgotPassword
      })
    }
  }

  render() {
    return (
      <Login
        userphone={this.props.phone}
        onPressSubmit={this.onPressSubmit}
        onPressForgotPassword={this.onPressForgotPassword}
        navigator={RNN}
        showLoading={this.state.showLoading}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
