import { StyleSheet, PixelRatio, Platform } from 'react-native'
import { Themes } from '../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: '#e0e4e7',
    backgroundColor: '#fff',
  },
  txtNumMoney: {
    flex: 1
  },
  searchInput: {
    height: 40,
    paddingHorizontal: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e4e7'
  }
})

export default styles
