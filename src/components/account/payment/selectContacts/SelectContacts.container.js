import React, { Component } from 'react'
import { connect } from 'react-redux'
import Config from '../../../../screens/config'
import { Colors } from '../../../../ui/themes'
import SelectContacts from './SelectContacts'
import { Navigation} from 'react-native-navigation'
class SelectContactsContainer extends Component {
  static navigatorStyle = {
    ...Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB,
    statusBarColor: Colors.navColor
  }

  constructor(props) {
    super(props)

  }

  render() {
    //console.log('selectContacts')
    //console.log(this.props)
    return (
      <SelectContacts
        navigator={Navigation}
        contacts={this.props.contacts}
        onContactPress={this.props.selectContact}
        componentId={this.props.componentId}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectContactsContainer)