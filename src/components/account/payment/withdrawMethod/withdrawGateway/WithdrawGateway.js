import React, { Component } from 'react'
import { TouchableWithoutFeedback, Keyboard, View, Image } from 'react-native'
import _ from 'lodash'
import { Themes, Button, TcombForm } from '../../../../../ui'
import styles from './withdrawGateway.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const payment = t.struct({
  account: t.String,
  money: t.String
})

class WithdrawGateway extends Component {

  constructor(props) {
    super(props)
    this.state = {
      value: {
        account: null,
        money: null
      }
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    const value = this.form.getValue()
    if (value) {
      let { money } = value
      money = money.replace(new RegExp('\\,', 'g'), '')
      this.props.onPressSubmit({ ...value, money })
    }
  }

  renderForm = () => {
    const options = {
      stylesheet: TcombForm.styles,
      fields: {
        account: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Số điện thoại Momo',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'numeric',
          config: {
            label: 'Tài khoản nhận tiền'
          }
        },
        money: {
          template: TcombForm.InputTextFormatMoney,
          placeholder: '50,000 đ',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'numeric',
          config: {
            label: 'Số tiền cần rút'
          }
        }
      }
    }
    return (
      <Form
        ref={(comp) => {
          this.form = comp
        }}
        type={payment}
        options={options}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  render() {
    const { paymentId } = this.props
    const iconName = _.toLower(`icon${paymentId}`)

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.container]}>
          <Image source={Themes.Images[iconName]} style={styles.momoImage} />
          {this.renderForm()}
          <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Tiếp tục</Button>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

WithdrawGateway.propTypes = {}
export default WithdrawGateway
