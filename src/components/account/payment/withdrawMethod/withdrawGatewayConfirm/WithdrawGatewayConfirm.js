import React, { Component } from 'react'
import { View, Image, ScrollView, TouchableOpacity } from 'react-native'
import { Themes, Button, Icon, TcombForm } from '../../../../../ui'
import styles from './withdrawGatewayConfirm.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const payment = t.struct({
  pin: t.Number
})

class WithdrawGateway extends Component {

  constructor(props) {
    super(props)
    this.state = {
      value: {
        pin: null,
      }
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    let value = this.form.getValue()
    if (value) {
      this.props.onPressSubmit(value)
    }
  }

  renderForm = () => {
    const options = {
      stylesheet: TcombForm.styles,
      fields: {
        pin: {
          template: TcombForm.InputTextNormal,
          placeholder: '******',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Mã PIN xác nhận'
          }
        },
      }
    }
    return (
      <Form
        ref={(comp) => {
          this.form = comp
        }}
        type={payment}
        options={options}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Xác nhận</Button>
      </View>
    )
  }
}
WithdrawGateway.propTypes = {}
export default WithdrawGateway
