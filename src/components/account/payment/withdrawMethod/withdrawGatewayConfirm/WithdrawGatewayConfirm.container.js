import React, { Component } from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import WithdrawGateway from './WithdrawGatewayConfirm'
import Config from '../../../../../screens/config'
import { Navigation } from '../../../../../screens/navigation'
import { actions as PaymentAction } from '../../../../../redux/payment'

class WithdrawGatewayContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  async onPressSubmit(value) {
    const { paymentAction } = this.props
    paymentAction.withdrawMomoConfirm({ ...value }, {
      nextScreen: (message) => {
        Alert.alert('Thông báo', message, [{
          text: 'Đồng ý',
          onPress: () => {
            Navigation.navToHome()
          }
        }])
      }
    })
  }

  render() {
    return (
      <WithdrawGateway
        onPressSubmit={this.onPressSubmit}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawGatewayContainer)
