import { StyleSheet, PixelRatio, Platform } from 'react-native'
import { Themes } from '../../../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  },
  momoImage: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginVertical: 10
  },
  txtNote: {
    marginBottom: 10,
    marginTop: -5
  },
  modal: {
    width: '85%',
    height: null,
    maxHeight: '100%'
  },
  btnTouchModal: {
    padding: 15,
    backgroundColor: '#E7E7E7',
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#ccc'
  },
  txtTax: {
    textAlign: 'center',
    marginTop: 10
  },
  txtTitleBank: {
    marginBottom: 15,
    marginTop: 15
  },
  content: {
    paddingBottom: 15,
    paddingLeft: 10,
    paddingTop: 5,
    paddingRight: 10,
  },
  headerText: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    paddingVertical: 10,
    paddingLeft: 10,
    backgroundColor: '#235789',
    color: '#fff'
  },
  active: {
    backgroundColor: '#FDFFFC'
  },
  inactive: {
    backgroundColor: '#FDFFFC'
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
    paddingBottom: 10,
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
    backgroundColor: 'red'
  },
  txtStore: {
    flex: 1,
    textAlign: 'right',
    paddingLeft: 10,
  },
  rowBank: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 5,
    justifyContent: 'center'
  }
})

export default styles
