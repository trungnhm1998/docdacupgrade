import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import DepositBankingInput from './DepositBankingInput'
import Config from '../../../../../../screens/config'
import { actions as PaymentAction } from '../../../../../../redux/payment'

class DepositBankingInputContainer extends Component {

  static navigatorStyle = Config.navigatorStyle.DEFAULT_NAV_STYLE_HIDDEN_TAB

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <DepositBankingInput
        bankInfo={this.props.payload.paymentInfo.bankInfo}
        navigator={this.props.navigator}
      />
    )
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentAction: bindActionCreators(PaymentAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositBankingInputContainer)
