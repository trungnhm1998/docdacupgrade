import React, { Component } from 'react'
import { View, Image, ScrollView, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modalbox'
import numeral from 'numeral'
import { Themes, TextFont, Button, Icon, TcombForm } from '../../../../../../ui'
import styles from './depositGatewayInput.style'
import _ from 'lodash'

const t = require('tcomb-form-native')
const Form = t.form.Form
const payment = t.struct({
  amount: t.Number
})

class DepositGatewayInput extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
      value: {
        amount: null,
      }
    }
    this.onPressSubmit = this.onPressSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onChange(value) {
    this.setState({ value })
  }

  onPressSubmit = () => {
    let value = this.form.getValue()
    if (value) {
      this.props.onPressSubmit(value)
    }
  }

  setValueForm = (key, value, modal) => {
    this.setState({
      value: {
        ...this.state.value,
        [key]: value
      }
    })
    this.refs[`${modal}`].close()
  }

  renderModalMoney = () => {
    const dataSelect = this.props.listPrice
    return (
      <Modal animationDuration={0} swipeToClose={false} style={[styles.modal]} position={'center'} ref={'modalMoney'}>
        <ScrollView bounces={false} contentContainerStyle={{ justifyContent: 'center' }}>
          {
            _.map(dataSelect, (data, index) => {
              return (
                <TouchableOpacity key={index} activeOpacity={0.7} onPress={() => this.setValueForm('amount', data, 'modalMoney')} style={[styles.btnTouchModal]}>
                  <TextFont color={data === this.state.value.amount ? Themes.Colors.navColor : null}>Nạp {numeral(data).format('0,0')} đ</TextFont>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </Modal>
    )
  }

  onFocusForm = (value) => {
    this.refs[`${value}`].open()
  }

  renderForm = () => {
    const { paymentName, feeDescription } = this.props

    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10 }}>
          <View style={{ marginBottom: 10 }}>{locals.inputs.amount}</View>
          <TextFont style={styles.txtNote} size={12} color={Themes.Colors.navColor}>* {feeDescription}</TextFont>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        amount: {
          factory: TcombForm.InputModalRightIcon,
          placeholder: 'Chọn số tiền',
          onFocus: () => this.onFocusForm('modalMoney'),
          placeholderTextColor: '#E7E7E7',
          config: {
            formatMoney: true,
            currency: 'đ',
            label: 'Số tiền cần nạp',
            nameIcon: 'down',
            IconSize: 7,
            IconColor: '#bcbcbc',
            labelStyle: {
              marginLeft: 0,
              color: '#222424'
            }
          }
        }
      }
    }
    return (
      <Form
        ref={(comp) => {
          this.form = comp
        }}
        type={payment}
        options={options}
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }

  render() {
    const { paymentId, paymentName } = this.props
    const iconName = _.toLower(`icon${paymentId}`)

    return (
      <View style={[styles.container]}>
        <Image source={Themes.Images[iconName]} style={styles.momoImage} />
        {this.renderForm()}
        <Button onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Nạp Tiền</Button>
        {this.renderModalMoney()}
      </View>
    )
  }
}
DepositGatewayInput.propTypes = {}
export default DepositGatewayInput
