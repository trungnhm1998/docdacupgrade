import { StyleSheet, PixelRatio, Platform } from 'react-native'
import { Themes } from '../../../../../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  },
  imgSuccess: {
    alignSelf: 'center',
    marginVertical: 25
  }
})

export default styles
