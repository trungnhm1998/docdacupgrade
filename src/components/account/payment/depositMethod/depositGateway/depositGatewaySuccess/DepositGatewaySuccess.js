import React, { Component } from 'react'
import { View, Image } from 'react-native'
import numeral from 'numeral'
import { Themes, TextFont, Button } from '../../../../../../ui'
import styles from './depositGatewaySuccess.style'

// const Fabric = require('react-native-fabric')

// const { Answers } = Fabric

class DepositGatewaySuccess extends Component {

  constructor(props) {
    super(props)
    this.state = {}
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }

  onPressSubmit = () => {
    this.props.onPressSubmit()
  }

  componentDidMount() {
    // const { amount, paymentType } = this.props 
    // Answers.logPurchase(amount, 'VND', true, paymentType, 'PAYMENT')
  }

  render() {
    const { amount } = this.props
    return (
      <View style={[styles.container]}>
        <Image style={[styles.imgSuccess]} source={Themes.Images.depositSuccess} />
        <View style={Themes.styleGB.centerContent}>
          <TextFont>Bạn đã nạp tiền thành công!</TextFont>
          <TextFont style={{ marginTop: 5 }} size={25} font={Themes.Fonts.type.Bold} color="#06b606">{numeral(amount).format('0,0')} đ</TextFont>
        </View>
        <Button style={{ marginTop: 20 }} onPress={this.onPressSubmit} btnColor={Themes.Colors.btnColor1}>Hoàn tất</Button>
      </View>
    )
  }
}
DepositGatewaySuccess.propTypes = {}
export default DepositGatewaySuccess
