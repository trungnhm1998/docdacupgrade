import { StyleSheet, PixelRatio, Platform, Dimensions } from 'react-native'
import { Themes } from '../../../ui/'

const { height, width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Themes.Metrics.paddingHorizontal,
    backgroundColor: '#f6f6f6',
    flex: 1
  },
  loginBg: {
    flex: 1,
    width: Themes.Metrics.screenWidth,
    backgroundColor: 'transparent',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  },
  btnTouch: {
    backgroundColor: 'red',
    height: 100,
    width: Themes.Metrics.screenWidth
  },
  loginContentTop: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  txtForgetPass: {
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginTop: 20
  },
  txtBottom: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: '#8b8b8b'
  },
  wrapperImg: {
    height: 115,
    marginHorizontal: -15
  },
  boxAvatar: {
    // flexDirection: 'row',
    // backgroundColor: '#fff',
    // height: 80,
    // paddingTop: 15,
    // paddingLeft: 20,
    position: 'absolute',
    backgroundColor: '#fff',
    left: 0,
    right: 0,
    top: 0,
    height: 70,
    zIndex: 2,
    justifyContent: 'flex-end'
  },
  avatarImage: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  btnAvatar: {
    marginTop: 15,
    width: 100,
    height: 100,
    marginLeft: 10,
    zIndex: 3
  },
  iconAvatar: {
    position: 'absolute',
    right: 0,
    bottom: 10,
    backgroundColor: 'transparent'
  },
  txtPhone: {
    alignSelf: 'flex-end',
    paddingRight: 30,
    paddingBottom: 5
  },
  imagePicker: {
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center'
  },
  buttonGroup: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width,
    paddingHorizontal: 10,
    marginTop: 30
  },
  actionAcceptButton: {
    width: 120,
    marginHorizontal: 10,
    backgroundColor: Themes.Colors.navColor,
    borderRadius: 25,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: Themes.Colors.navColor
  },
  actionExitButton: {
    width: 120,
    marginHorizontal: 10,
    backgroundColor: '#fff',
    borderRadius: 25,
    borderColor: Themes.Colors.navColor,
    borderWidth: 1 / PixelRatio.get()
  },
  containerPicker: {
    flex: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
  buttonAcceptStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
    lineHeight: 16,
    paddingVertical: 0,
    marginVertical: 0,
    paddingBottom: 3
  },
  buttonExitStyle: {
    color: Themes.Colors.navColor,
    textAlign: 'center',
    fontSize: 16,
    lineHeight: 16,
    paddingVertical: 0,
    marginVertical: 0,
    paddingBottom: 3
  }
})

export default styles
