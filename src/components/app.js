import React, { Component } from 'react'
// import { bindActionCreators } from 'redux'
// import { connect } from 'react-redux'
import { Text, View, Button } from 'react-native'
// import { Navigation } from '../screens/navigation'

export default class App extends Component {
  foo() {
    // Navigation.navToTest()
  }

  render() {
    return (
      <View>
        <Button
          onPress={() => this.foo()}
          title="Set global state"
          color="red"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    )
  }
}

// const mapStateToProps = (rootState) => {
//   //console.log(rootState)
//   return {
//   }
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(App)
