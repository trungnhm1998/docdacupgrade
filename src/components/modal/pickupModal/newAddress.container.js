import React, { Component } from 'react'
import { Keyboard, ScrollView, TouchableOpacity, View, Platform, Alert } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import _ from 'lodash'
import Picker from 'react-native-picker'
import * as AccountAction from '../../../redux/account/account.actions'
import { types } from '../../../redux/account/'
import { Navigation } from '../../../screens/navigation'
import styles from './address.style'
import { hideKeyboard, Icon, TcombForm, TextFont, Themes, ThreeDButton, Overlay } from '../../../ui'

const t = require('tcomb-form-native')

const Form = t.form.Form
const userAddress = t.struct({
  name: t.String,
  phone: t.String,
  province: t.String,
  district: t.String,
  ward: t.String,
  address: t.String
})

class NewAddress extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    navBarTranslucent: true,
    screenBackgroundColor: 'transparent',
    modalPresentationStyle: 'overCurrentContext'
  }

  constructor(props) {
    super(props)
    this.state = {
      value: {},
      visibleOverlay: false
    }
    this.closeModal = this.closeModal.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  componentWillMount() {
    hideKeyboard()
  }

  onChange(value) {
    this.setState({ value })
  }

  onSubmit() {
    const value = this.form.getValue()
    if (value) {
      const { accountAction } = this.props
      const { accessToken } = this.props.payload.accountInfo
      const { provinceId, districtId, wardId } = this.state
      const { name, phone, address, province, district, ward } = value

      accountAction.addNewAddress({
        province_id: provinceId,
        province_name: province,
        district_id: districtId,
        district_name: district.replace('Quận ', ''),
        ward_id: wardId,
        ward_name: ward.replace('Phường ', ''),
        receiver_name: name,
        receiver_phone: phone,
        address
      }, accessToken)

      this.props.onPressSubmit({
        client_name: name,
        client_phone: phone,
        address_id: null,
        address,
        province_id: provinceId,
        province_name: province,
        district_id: districtId,
        district_name: district.replace('Quận ', ''),
        ward_id: wardId,
        ward_name: ward.replace('Phường ', '')
      })
    } else {
      Alert.alert('Thông báo', 'Bạn vui lòng nhập đầy đủ thông tin.')
    }
  }

  closeModal = () => {
    this.props.onCloseModalAddress()
    this.props.navigator.dismissModal({
      animationType: 'none'
    })
  }

  renderForm = () => {
    const options = {
      stylesheet: TcombForm.styles,
      fields: {
        name: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập họ và tên',
          autoCapitalize: 'characters',
          placeholderTextColor: '#E7E7E7',
          onFocus: () => {
            Picker.hide()
          }
        },
        phone: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Số điện thoại',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad',
          config: {},
          onFocus: () => {
            Picker.hide()
          }
        },
        province: {
          template: TcombForm.InputRightIcon,
          placeholder: 'Chọn tỉnh/ Thành phố',
          placeholderTextColor: '#E7E7E7',
          config: {
            rightIcon: 'dropdown',
            rightIconSize: 5
          },
          onFocus: () => {
            Keyboard.dismiss()
            this.showProvince()
          }
        },
        district: {
          template: TcombForm.InputRightIcon,
          placeholder: 'Quận Huyện',
          placeholderTextColor: '#E7E7E7',
          config: {
            rightIcon: 'dropdown',
            rightIconSize: 5
          },
          onFocus: () => {
            if (!this.state.value.province) {
              Alert.alert('Thông báo', 'Vui lòng chọn thành phố.')
            } else {
              Keyboard.dismiss()
              this.showDistrict()
            }
          }
        },
        ward: {
          template: TcombForm.InputRightIcon,
          placeholder: 'Phường xã',
          placeholderTextColor: '#E7E7E7',
          config: {
            rightIcon: 'dropdown',
            rightIconSize: 5
          },
          onFocus: () => {
            if (!this.state.value.province) {
              Alert.alert('Thông báo', 'Vui lòng chọn thành phố.')
            } else if (!this.state.value.district) {
              Alert.alert('Thông báo', 'Vui lòng chọn quận.')
            } else {
              Keyboard.dismiss()
              this.showWard()
            }
          }
        },
        address: {
          template: TcombForm.InputTextNormal,
          placeholder: 'Nhập địa chỉ',
          placeholderTextColor: '#E7E7E7',
          config: {}
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        onChange={this.onChange}
        value={this.state.value}
        type={userAddress}
        options={options}
      />
    )
  }

  showProvince() {
    const listProvince = []
    const { deliveryLocation } = this.props

    _.forEach(deliveryLocation, (item) => {
      listProvince.push(item.province_name)
    })

    Picker.init({
      pickerData: listProvince,
    //  selectedValue: [this.state.value.province],
      selectedValue: [],
      pickerTitleText: 'Thành phố',
      pickerTitleColor: [255, 255, 255, 20],
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerConfirm: (value) => {
        const selectedProvince = _.find(deliveryLocation, (item) => {
          return item.province_name === value[0]
        })
        if (selectedProvince) {
          this.setState({ value: { ...this.state.value, province: value[0], district: '', ward: '' }, provinceId: selectedProvince.province_id })
        }
      }
    })
    Picker.show()
  }

  showDistrict() {
    const listDistrict = []
    const { deliveryLocation } = this.props
    const { provinceId } = this.state
    if (!provinceId) {
      return
    }

    const selectedProvince = _.find(deliveryLocation, (item) => {
      return item.province_id === provinceId
    })

    _.forEach(selectedProvince.district, (item) => {
      listDistrict.push(`${item.type} ${item.name}`)
    })

    Picker.init({
      pickerData: listDistrict,
     // selectedValue: [this.state.value.district],
      selectedValue: [],
      pickerTitleText: 'Quận',
      pickerTitleColor: [255, 255, 255, 20],
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerConfirm: (value) => {
        const selectedDistrict = _.find(selectedProvince.district, (item) => {
          return `${item.type} ${item.name}` === value[0]
        })

        if (selectedDistrict) {
          this.setState({ value: { ...this.state.value, district: value[0], ward: '' }, districtId: selectedDistrict.district_id })
        }
      }
    })
    Picker.show()
  }

  showWard() {
    const listDistrict = []
    const { deliveryLocation } = this.props
    const { provinceId, districtId } = this.state
    if (!districtId || !provinceId) {
      return
    }

    const selectedProvince = _.find(deliveryLocation, (item) => {
      return item.province_id === provinceId
    })

    const selectedDistrict = _.find(selectedProvince.district, (item) => {
      return item.district_id === districtId
    })

    _.forEach(selectedDistrict.ward, (item) => {
      listDistrict.push(`Phường ${item.ward_name}`)
    })

    Picker.init({
      pickerData: listDistrict,
     // selectedValue: [this.state.value.district],
      selectedValue: [],
      pickerTitleText: 'Phường',
      pickerTitleColor: [255, 255, 255, 20],
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerConfirm: (value) => {
        const selectedWard = _.find(selectedDistrict.ward, (item) => {
          return `Phường ${item.ward_name}` === value[0]
        })

        if (selectedWard) {
          this.setState({ value: { ...this.state.value, ward: value[0] }, wardId: selectedWard.ward_id })
        }
      }
    })
    Picker.show()
  }

  render() {
    return (
      <View style={styles.screen}>
        <ScrollView style={styles.scrollView} contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.modalContainerNewAddress}>
            <View style={styles.header}>
              <TouchableOpacity style={styles.buttonHeader} />
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <TextFont style={{ textAlign: 'center' }}>TẠO ĐỊA CHỈ MỚI</TextFont>
              </View>
              <TouchableOpacity onPress={this.closeModal} style={styles.buttonHeader}>
                <Icon name={'close'} size={20} color="#797979" />
              </TouchableOpacity>
            </View>
            <View style={styles.contentContainer}>
              {this.renderForm()}
              <ThreeDButton onPress={this.onSubmit} text={'Thêm địa chỉ'} />
            </View>
          </View>
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        </ScrollView>
      </View>
    )
  }

  componentDidUpdate() {
    const { navigator, payload } = this.props
    const { apiResponse } = payload
    if (apiResponse.type === types.ADD_NEW_ADDRESS_SUCCESS) {
      Alert.alert('Thông báo', 'Tạo địa chỉ mới thành công', [{ 
        text: 'Đồng ý',
        onPress: () => {
          navigator.dismissModal({
            animationType: 'none'
          })
        } 
      }])
    }
  }
}

const mapStateToProps = (rootState) => {
  return {
    payload: rootState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    accountAction: bindActionCreators(AccountAction, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewAddress)
