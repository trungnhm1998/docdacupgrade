import React, { Component } from 'react'
import { FlatList, Platform, ScrollView, TouchableOpacity, View } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import * as ApiAction from '../../../redux/api/api.actions'
import styles from './address.style'
import { hideKeyboard, Icon, TextFont, ThreeDButton } from '../../../ui'

class AddressList extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    navBarTranslucent: true,
    screenBackgroundColor: 'transparent',
    modalPresentationStyle: 'overCurrentContext'
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: '',
      selectedAddress: null
    }
    this.closeModal = this.closeModal.bind(this)
    this.onNewAddress = this.onNewAddress.bind(this)
    this.onPressSubmit = this.onPressSubmit.bind(this)
  }


  componentWillMount() {
    hideKeyboard()
  }

  onPressSubmit() {
    const { selectedAddress } = this.state
    this.props.onCloseModalAddress(selectedAddress)
    if (selectedAddress) {
      this.props.onPressSubmit(selectedAddress)
      this.props.navigator.dismissModal({
        animationType: 'none'
      })
    }
  }

  onPressItem = (item) => {
    this.setState({
      selected: item.address_id,
      selectedAddress: item
    })
  }

  closeModal = () => {
    this.props.onCloseModalAddress()
    this.props.navigator.dismissModal({
      animationType: 'none'
    })
  }

  onNewAddress = () => {
    const { deliveryLocation } = this.props
    this.props.navigator.dismissModal({
      animationType: 'none'
    })
    setTimeout(() => {
      this.props.navigator.showModal({
        screen: 'NewAddress',
        backButtonTitle: '',
        passProps: {
          deliveryLocation,
          onPressSubmit: this.props.onPressSubmit,
          onCloseModalAddress: this.props.onCloseModalAddress
        },
        style: {
          backgroundBlur: 'none',
          backgroundColor: 'rgba(0,0,0,0.5)'
        },
        animationType: 'none'
      })
    }, 50)
  }

  addressItem = (props) => {
    return (
      <TouchableOpacity onPress={() => this.onPressItem(props.item)} class="ratioItemList" style={styles.ratioItemList}>
        <View style={styles.ratioItem}>
          <View
            class="RatioSelected"
            style={[styles.selectRatio, this.state.selected === props.item.address_id ? styles.selectedRatio : '']}
          />
        </View>
        <View style={styles.ratioTextLabel}>
          <TextFont style={styles.buyerName} font={'B'}>{props.item.client_name}</TextFont>
          <TextFont style={styles.address} numberOfLines={1} ellipsizeMode="tail" font={'R'}>{props.item.address}, P.{props.item.ward_name}, Q.{props.item.district_name}, {props.item.province_name}</TextFont>
          <TextFont style={styles.address} font={'L'}>SĐT liên hệ: {props.item.client_phone}</TextFont>
        </View>
      </TouchableOpacity>
    )
  }


  render() {
    if (!this.props.userAddressList) {
      return <View />
    }

    const userAddressList = Object.values(this.props.userAddressList)
    return (
      <View style={styles.screen}>
        <View style={styles.modalContainer}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.buttonHeader} />
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <TextFont style={{ textAlign: 'center' }}>CHỌN ĐỊA CHỈ NHẬN VÉ</TextFont>
            </View>
            <TouchableOpacity onPress={this.closeModal} style={styles.buttonHeader}>
              <Icon name={'close'} size={20} color="#797979" />
            </TouchableOpacity>
          </View>
          <View style={styles.contentContainer}>
            <FlatList
              data={userAddressList}
              keyExtractor={item => item.address_id}
              extraData={this.state}
              renderItem={(item) => {
                return this.addressItem(item)
              }}
            />
            <TouchableOpacity onPress={() => this.onNewAddress()} style={[styles.ratioItemList, { marginBottom: 15 }]}>
              <View style={styles.ratioItem}>
                <View
                  class="RatioSelected"
                  style={[styles.selectRatio]}
                />
              </View>
              <View style={styles.ratioTextLabel}>
                <TextFont style={styles.buyerName} font={'B'}>Tạo địa chỉ mới</TextFont>
              </View>
            </TouchableOpacity>
            <ThreeDButton text={'Chọn'} onPress={this.onPressSubmit} />
          </View>
        </View>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </View>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    apiAction: bindActionCreators(ApiAction, dispatch)
  }
}
export default connect(null, mapDispatchToProps)(AddressList)
