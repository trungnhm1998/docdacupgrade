import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { Navigation } from 'react-native-navigation'
import styles from './loading.styles'
import { hideKeyboard } from '../../../ui'
import * as ApiAction from '../../../redux/api/api.actions'

class LightBoxScreen extends Component {
  componentDidMount() {
    this.navigationEventListener = Navigation.events().bindComponent(this)
  }

  componentWillUnmount() {
    // Not mandatory
    this.props.apiAction.setLoadingId('')
    if (this.navigationEventListener) {
      this.navigationEventListener.remove()
    }
  }

  componentDidAppear() {
    //console.log('loading did appear', this.props.componentId)
    this.props.apiAction.setLoadingId(this.props.componentId)
  }

  constructor(props) {
    super(props)
  }

  componentWillMount() {
    hideKeyboard()
    this.props.apiAction.setLoadingId(this.props.componentId)
  }

  render() {
    return (
      <View style={styles.screen}>
        <ActivityIndicator
          size="large"
        />
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    apiAction: bindActionCreators(ApiAction, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(LightBoxScreen)
