import React, { Component } from 'react'
import {
  Alert, Image, View, ScrollView, TouchableOpacity, Dimensions, Linking, Platform 
} from 'react-native'
import Carousel from 'react-native-snap-carousel'
import moment from 'moment'
import _ from 'lodash'
import numeral from 'numeral'
import { Navigation } from 'react-native-navigation'
import DeviceInfo from 'react-native-device-info'
import {
  Button, LogoNav, TextFont, Themes, Loading, StatusBar 
} from '../../ui'
import Config from '../../screens/config'
import styles from './home.style'
import { helper } from '../../utils'
import Countdown from '../../utils/countDown'
import store from '../../configs/store.config'
import { VERSION } from '../../configs/version.config'
import { PURCHASE_WAP } from '../../configs/api.config'

const { width } = Dimensions.get('window')

class Home extends Component {
  constructor(props) {
    super(props)
    this.renderBannerItem = this.renderBannerItem.bind(this)
    
  }

  getResource(ticket_type) {
    switch (ticket_type) {
      case '1':
        return {
          logo: Themes.Images.logo655,
          style: styles.higher655
        }
      case '2':
        return {
          logo: Themes.Images.logo645,
          style: styles.higherRowRed
        }
      case '4':
        return {
          logo: Themes.Images.logoMax4d,
          style: styles.higherRowPurple
        }
      default:
        return ''
    }
  }

  onPressBuyTicket=(ticket_type)=> {
    const { accountInfo } = this.props;
    //console.log('test press buy',accountInfo)
    const reviewStore = accountInfo.moneySetting.data.find((element) => {
      return element.name === 'in_review'
    })

    if (Platform.OS === 'ios' &&
        // accountInfo.production_version === VERSION // TrungNguyen Comment for new way to check
        reviewStore.value === DeviceInfo.getVersion()
        ) {
      const purchaseUrl = `${PURCHASE_WAP}?token=${accountInfo.accessToken}`
      Linking.canOpenURL(purchaseUrl).then(() => {
        Linking.openURL(purchaseUrl)
      }).catch(err => console.warn('An unexpected error happened', err))
      return null
    }

    switch (ticket_type) {
      case '1':
        this.props.onPressMegaPower()
        break
      case '2':
        this.props.onPressMega()
        break
      case '4':
        this.props.onPressMax4d()
        break
      default:
        return ''
    }
    return null
  }
  
  renderBuyButton({ ticket_type, time_off_service }) {
    const { start, end } = time_off_service

    const timeStart = moment(start, 'HH:mm').format()
    const timeEnd = moment(end, 'HH:mm').format()
    const timeOffService = moment().isBetween(timeStart, timeEnd)


    if ((parseInt(start, 10) === -1 && parseInt(end, 10) === -1) || timeOffService === false) {
      return (
        <Button
          onPress={() => { this.onPressBuyTicket(ticket_type) }}
          style={styles.actionButton}
          btnColor="#fdb200"
        >
          MUA VÉ NGAY
        </Button>
      )
    }

    return <Button style={styles.actionButton} btnColor="#fdb200">TẠM NGƯNG BÁN VÉ</Button>
  }

  renderGame() {
    const { ticketPrizeInfo } = this.props

    return _.map(ticketPrizeInfo, (item, key) => {
      const {
        ticket_type, next_drawing, prize, enable 
      } = item
      if (parseInt(enable, 10) !== 1) {
        return null
      }
      
      const options = { endDate: moment(next_drawing, 'HH:mm DD-MM-YYYY').format('MM-DD-YYYY HH:mm'), prefix: '', cb: () => {} }
      const resource = this.getResource(ticket_type)

      return (
        <View key={_.uniqueId()} style={styles.roundBox}>
          <View style={[styles.rowBox, resource.style]}>
            <View style={styles.leftCol}>
              <Image source={resource.logo} style={styles.logoStyle} />
              <Countdown options={options} />
            </View>
            <View style={[styles.rightCol]}>
              <TextFont style={styles.jackpotPrice}>
                {numeral(prize).format('0,0')}
                <TextFont style={styles.currency}> đ</TextFont>
              </TextFont>
              {this.renderBuyButton(item)}
            </View>
          </View>
          <View style={[styles.rowBox, styles.bottomRow]}>
            <View style={[styles.footerText, { alignItems: 'flex-start' }]}>
              <TextFont
                onPress={() => {
                  this.props.navigator.push(this.props.componentId, {
                    component: {
                      ...Config.screen.settingHomeIntro,
                      passProps: { uri: 'https://docdac.vn/gioi-thieu.html' },
                      options: {
                        topBar: {
                          visible: true,
                          drawBehind: false,
                          noBorder: true,
                          background: {
                            color: Themes.Colors.navColor
                          },
                          title: {
                            color: '#fff',
                            text: 'Giới thiệu cách chơi'
                          },
                          backButton: {
                            title: '',
                            color:'#fff'
                          }
                        },
                        layout: {
                          backgroundColor: 'transparent'
                        }
                      }
                    }
                  })
                }}
                style={styles.footerButton}
              >
                Hướng dẫn chơi
              </TextFont>
            </View>
            <View style={[styles.footerText, { alignItems: 'flex-end' }]}>
              <TextFont
                onPress={() => {
                  this.props.onPressLotteryResult(parseInt(key, 10))
                }}
                style={styles.footerButton}
              >
                Các kỳ quay số
              </TextFont>
            </View>
          </View>
        </View>
      )
    })
  }

  renderBackupBanner() {
    return (
      <TouchableOpacity
        activeOpacity={1}
        focusedOpacity={1}
        onPress={() => {
          this.props.navigator.push({
            ...Config.screen.settingHomeIntro,
            // title: 'Giới thiệu cách chơi',
            passProps: { uri: 'https://docdac.vn/gioi-thieu.html' },
            // options: {
            //   topBar: {
            //     visible: true,
            //     drawBehind: false,
            //     noBorder: true,
            //     background: {
            //       color: Themes.Colors.navColor
            //     },
            //     title: {
            //       color: '#fff',
            //       text: 'Giới thiệu cách chơi'
            //     },
            //     backButton: {
            //       title: ''
            //     }
            //   },
            //   layout: {
            //     backgroundColor: 'transparent'
            //   }
            // }
          })
        }}
        style={{ flex: 1 }}
      >
        <Image
          source={Themes.Images.homeBanner}
          style={styles.headerBanner}
        />
      </TouchableOpacity>
    )
  }

  renderBannerItem(data) {
    return (
      <View
        key={data.index}
        style={styles.headerBanner}
      >
        <TouchableOpacity
          // style={styles.headerBanner}
          activeOpacity={1}
          focusedOpacity={1}
          onPress={() => {
            //console.log('data.item', data.item)
            if (_.startsWith(data.item.deeplink, 'http')) {
              Linking.openURL(data.item.deeplink)
              /* this.props.navigator.push({
                ...Config.screen.settingInfo,
                title: data.description,
                passProps: { uri: data.deeplink }
              }) */
            } else {
              const page = helper.getParameterByName('page', data.item.deeplink)
              this.props.navigator.push(this.props.componentId, {
                component: {
                  ...Config.screen[page]
                }
              })
            }
          }}
        >
          <Image
            source={{ uri: this.props.mediaUrl + data.item.image_url }}
            style={[styles.headerBanner, { resizeMode: 'contain' }]}
          />
        </TouchableOpacity>
      </View>
    )
  }

  renderBannerList() {
    const { bannerList } = this.props
    return (
      <Carousel
        sliderWidth={width}
        itemWidth={width}
        inactiveSlideScale={1}
        autoplay
        data={this.props.bannerList}
        renderItem={this.renderBannerItem}
      />
    )
  }

  render() {
    if (this.props.isFetching === true) {
      return (<Loading />)
    }
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="rgba(0,0,0,0)" translucent />
        <LogoNav imageSouce={Themes.Images.docdacLogo} />
        <ScrollView>
          {_.isNil(this.props.bannerList) || this.props.bannerList.length === 0 ? this.renderBackupBanner() : this.renderBannerList()}
          <View style={styles.contentBox}>
            {this.renderGame()}
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Home
