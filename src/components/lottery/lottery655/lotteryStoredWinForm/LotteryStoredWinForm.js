import React, { Component } from 'react'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment'
import _ from 'lodash'
import Picker from 'react-native-picker'
import { View, Alert } from 'react-native'
import { Themes, TextFont, Icon, Button, TcombForm, Overlay } from '../../../../ui'
import styles from './lotterystoredwinform.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  location: t.String,
  store: t.String,
  date: t.String
})

class LotteryStoredWinForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDatePicker: false,
      isTimePicker: false,
      formValue: {
        location: null,
        store: null,
        date: null,
        ticketID: parseInt(this.props.ticketId, 0)
      }
    }
    this.onPressWinForm = this.onPressWinForm.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (_.isNil(nextProps.agentList)) {
      this.setState({
        formValue: {
          ...this.state.formValue,
          store: null
        }
      })
    }
  }

  showAddress() {
    const { deliveryLocation } = this.props
    const data = []
    _.forEach(deliveryLocation, (item) => {
      const provinceName = item.province_name
      const itemObj = {}
      itemObj[provinceName] = _.map(item.district, itemDistrict => `${itemDistrict.type} ${itemDistrict.name}`)
      data.push(itemObj)
    })
    const selectedItem = _.isNil(this.state.formValue) ? [] : [this.state.formValue.province, this.state.formValue.district]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedProvince = _.find(deliveryLocation, (item) => {
          return item.province_name === value[0]
        })

        if (selectedProvince) {
          const selectedDistrict = _.find(selectedProvince.district, (item) => {
            return item.name === _.trim(value[1], [item.type, ' '])
          })
          this.props.getAgentList(selectedDistrict.district_id)

          this.setState({
            visibleOverlay: false,
            formValue: {
              ...this.state.formValue,
              province: value[0],
              district: value[1],
              districtId: selectedDistrict.district_id,
              provinceId: selectedProvince.province_id,
              location: `${value[0]} - ${value[1]}`
            }
          })
        }
      }
    })
    Picker.show()
  }

  showAgent() {
    const error = _.isNil(this.state.formValue) || _.isNil(this.props.agentList) ? 'Bạn chưa chọn khu vực lấy vé hoặc không có đại lý tại nơi bạn đã chọn' : null
    if (!_.isNull(error)) {
      Alert.alert('Thông báo', error)
      return false
    }
    const { agentList } = this.props
    const data = []
    _.forEach(agentList, (item) => {
      const agentName = item.agent_name
      data.push(agentName)
    })

    const selectedItem = _.isNil(this.state.agentName) ? [] : [this.state.agentName]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedAgent = _.find(agentList, (item) => {
          return item.agent_name === value[0]
        })
        this.setState({
          visibleOverlay: false,
          formValue: {
            ...this.state.formValue,
            agentId: parseInt(selectedAgent.user_id, 0),
            store: value[0]
          }
        })
      }
    })
    Picker.show()
  }

  onPressWinForm = () => {
    const value = this.form.getValue()
    if (value) {
      const data = {
        ...value,
        ...this.state.formValue
      }
      this.props.onPressWinForm(data)
    }
  }

  statusDatePicker = () => { this.setState({ isDatePicker: !this.state.isDatePicker }) }
  statusTimePicker = () => { this.setState({ isTimePicker: !this.state.isTimePicker }) }

  handleDatePicked = (date) => {
    const dateNew = moment(date).format('DD-MM-YYYY')
    const dateValue = moment(date).format('YYYY-MM-DD HH:mm:ss')
    this.setState({
      formValue: {
        ...this.state.formValue,
        date: dateNew,
        dateValue
      }
    })
    this.statusDatePicker()
  }

  handleTimePicked = (time) => {
    const timeNew = moment(time).format('HH:mm A')
    this.setState({
      formValue: {
        ...this.state.formValue,
        hour: timeNew
      }
    })
    this.statusTimePicker()
  }

  renderFormLocation = () => {
    const layout = (locals) => {
      return (
        <View>
          <View style={{ backgroundColor: '#fff', padding: 10 }}>
            <View style={{ marginBottom: 10 }}>{locals.inputs.location}</View>
            <View>{locals.inputs.store}</View>
          </View>
          <View style={[styles.rowWinForm]}>
            <Icon name="circle" color={Themes.Colors.navColor} />
            <TextFont style={[styles.txtWinForm]}>Thời gian nhận thưởng</TextFont>
          </View>
          <View style={{ flexDirection: 'row', padding: 10 }}>
            <View style={{ flex: 1, paddingRight: 5 }}>{locals.inputs.date}</View>
            {/* <View style={{ flex: 1, paddingLeft: 5 }}>{locals.inputs.hour}</View> */}
          </View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        location: {
          template: TcombForm.InputModalLeftIcon,
          onFocus: () => this.showAddress(),
          placeholder: 'Chọn khu vực nhận thưởng',
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.location,
            label: '',
            nameIcon: 'location',
            IconSize: 17,
            IconColor: '#9f9f9f'
          }
        },
        store: {
          template: TcombForm.InputModalLeftIcon,
          onFocus: () => this.showAgent(),
          placeholder: 'Đại lý',
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.store,
            nameIcon: 'store',
            IconSize: 17,
            IconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        },
        date: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Ngày nhận vé',
          onFocus: () => this.statusDatePicker(),
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.date,
            label: '',
            nameIcon: 'list',
            IconSize: 17,
            IconColor: '#9f9f9f'
          }
        },
        hour: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Giờ nhận vé',
          onFocus: () => this.statusTimePicker(),
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.hour,
            label: '',
            nameIcon: 'history',
            IconSize: 17,
            IconColor: '#9f9f9f'
          }
        }
      }
    }
    return (
      <Form ref={(form) => { this.form = form }} value={this.state.formValue} type={userAccount} options={options} />
    )
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={[styles.rowWinForm]}>
          <Icon name="circle" color={Themes.Colors.navColor} />
          <TextFont style={[styles.txtWinForm]}>Vé trúng trưởng.</TextFont>
        </View>
        <View style={[styles.rowWinForm]}>
          <Icon name="circle" color={Themes.Colors.navColor} />
          <TextFont style={[styles.txtWinForm]}>Chọn đại lý: </TextFont>
        </View>
        {this.renderFormLocation()}

        <Button onPress={this.onPressWinForm} style={[styles.btnConfirm]}>ĐĂNG KÝ NHẬN THƯỞNG</Button>

        <TextFont color="#727272" style={Themes.styleGB.textCenter} size={12}>* Không được thấp hơn 2 ngày không bao gồm ngày nghỉ</TextFont>

        <DateTimePicker
          isVisible={this.state.isDatePicker}
          onConfirm={this.handleDatePicked}
          onCancel={this.statusDatePicker}
        />
        <DateTimePicker
          isVisible={this.state.isTimePicker}
          onConfirm={this.handleTimePicked}
          onCancel={this.statusTimePicker}
          mode="time"
        />
        <Overlay
          visible={this.state.visibleOverlay}
          onPressOverlay={() => {
            Picker.hide()
            this.setState({ visibleOverlay: false })
          }}
        />

      </View>
    )
  }

}
LotteryStoredWinForm.propTypes = {}
export default LotteryStoredWinForm
