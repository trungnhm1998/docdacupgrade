import React, { Component } from 'react'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment'
import _ from 'lodash'
import Picker from 'react-native-picker'
import { View, Alert } from 'react-native'
import { Themes, TextFont, Icon, Button, TcombForm, Overlay } from '../../../../ui'
import styles from './lotterystoredwinform.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  store: t.String,
  date: t.String
})

class LotteryStoredWinForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDatePicker: false,
      isTimePicker: false,
      formValue: {
        store: null,
        date: null,
        ticketID: parseInt(this.props.ticketId, 0)
      }
    }
    this.onPressWinForm = this.onPressWinForm.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (_.isNil(nextProps.agentReward)) {
      this.setState({
        formValue: {
          ...this.state.formValue,
          store: null
        }
      })
    }
  }

  showAgent() {
    const { addresses } = this.props.agentReward
    const data = []
    _.forEach(addresses, (item) => {
      const agentName = item.name
      data.push(agentName)
    })

    const selectedItem = _.isNil(this.state.formValue.store) ? [] : [this.state.formValue.store]
    this.setState({ visibleOverlay: true })
    Picker.init({
      pickerData: data,
      selectedValue: selectedItem,
      pickerTitleText: '',
      pickerToolBarBg: [201, 32, 51, 1],
      pickerBg: [255, 255, 255, 20],
      pickerConfirmBtnColor: [255, 255, 255, 1],
      pickerCancelBtnColor: [255, 255, 255, 1],
      pickerConfirmBtnText: 'Chọn',
      pickerCancelBtnText: 'Hủy',
      onPickerCancel: () => {
        this.setState({
          visibleOverlay: false
        })
      },
      onPickerConfirm: (value) => {
        const selectedAgent = _.find(addresses, (item) => {
          return item.name === value[0]
        })
        this.setState({
          visibleOverlay: false,
          formValue: {
            ...this.state.formValue,
            selectedAgent,
            store: value[0]
          }
        })
      }
    })
    Picker.show()
  }

  onPressWinForm = () => {
    const value = this.form.getValue()
    if (value) {
      const data = {
        ...value,
        ...this.state.formValue
      }
      this.props.onPressWinForm(data)
    }
  }

  statusDatePicker = () => { this.setState({ isDatePicker: !this.state.isDatePicker }) }
  statusTimePicker = () => { this.setState({ isTimePicker: !this.state.isTimePicker }) }

  handleDatePicked = (date) => {
    const dateNew = moment(date).format('DD-MM-YYYY')
    const dateValue = moment(date).format('YYYY-MM-DD HH:mm:ss')
    this.setState({
      formValue: {
        ...this.state.formValue,
        date: dateNew,
        dateValue
      }
    })
    this.statusDatePicker()
  }

  handleTimePicked = (time) => {
    const timeNew = moment(time).format('HH:mm A')
    this.setState({
      formValue: {
        ...this.state.formValue,
        hour: timeNew
      }
    })
    this.statusTimePicker()
  }

  renderFormLocation = () => {
    const { selectedAgent } = this.state.formValue
    const layout = (locals) => {
      return (
        <View>
          <View style={{ backgroundColor: '#fff', padding: 10 }}>
            <View>{locals.inputs.store}</View>
            <TextFont style={{ fontSize: 12, marginTop: 10 }}>{selectedAgent ? `Địa chỉ: ${selectedAgent.address}` : '' }</TextFont>
          </View>
          <View style={[styles.rowWinForm]}>
            <Icon name="circle" color={Themes.Colors.navColor} />
            <TextFont style={[styles.txtWinForm]}>Thời gian nhận thưởng</TextFont>
          </View>
          <View style={{ flexDirection: 'row', padding: 10 }}>
            <View style={{ flex: 1, paddingRight: 5 }}>{locals.inputs.date}</View>
            {/* <View style={{ flex: 1, paddingLeft: 5 }}>{locals.inputs.hour}</View> */}
          </View>
        </View>
      )
    }
    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        store: {
          template: TcombForm.InputModalLeftIcon,
          onFocus: () => this.showAgent(),
          placeholder: 'Đại lý',
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.store,
            nameIcon: 'store',
            IconSize: 17,
            IconColor: '#9f9f9f',
            inputStyle: {
              marginBottom: 0,
              borderRadius: 24
            }
          }
        },
        date: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Ngày nhận vé',
          onFocus: () => this.statusDatePicker(),
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.date,
            label: '',
            nameIcon: 'list',
            IconSize: 17,
            IconColor: '#9f9f9f'
          }
        },
        hour: {
          template: TcombForm.InputModalLeftIcon,
          placeholder: 'Giờ nhận vé',
          onFocus: () => this.statusTimePicker(),
          placeholderTextColor: '#E7E7E7',
          config: {
            value: this.state.formValue.hour,
            label: '',
            nameIcon: 'history',
            IconSize: 17,
            IconColor: '#9f9f9f'
          }
        }
      }
    }
    return (
      <Form ref={(form) => { this.form = form }} value={this.state.formValue} type={userAccount} options={options} />
    )
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={[styles.rowWinForm]}>
          <Icon name="circle" color={Themes.Colors.navColor} />
          <TextFont style={[styles.txtWinForm]}>Chọn địa điểm: </TextFont>
        </View>
        {this.renderFormLocation()}
        <Button onPress={this.onPressWinForm} style={[styles.btnConfirm]}>ĐĂNG KÝ NHẬN THƯỞNG</Button>
        <TextFont color="#727272" style={Themes.styleGB.textCenter} size={12}>* Không được thấp hơn 2 ngày không bao gồm ngày nghỉ</TextFont>
        <DateTimePicker
          isVisible={this.state.isDatePicker}
          onConfirm={this.handleDatePicked}
          onCancel={this.statusDatePicker}
        />
        <Overlay
          visible={this.state.visibleOverlay}
          onPressOverlay={() => {
            Picker.hide()
            this.setState({ visibleOverlay: false })
          }}
        />

      </View>
    )
  }

}
LotteryStoredWinForm.propTypes = {}
export default LotteryStoredWinForm
