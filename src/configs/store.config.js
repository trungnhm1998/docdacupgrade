import { createStore, applyMiddleware, compose } from 'redux'
import { AsyncStorage } from 'react-native'
import createSagaMiddleware from 'redux-saga'
import { persistReducer, persistStore } from 'redux-persist'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'remote-redux-devtools'
import rootReducers from './reducers.config'
import rootSaga from '../sagas'

/* eslint-disable no-underscore-dangle */
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const composeEnhancers = composeWithDevTools({ realtime: true, port: 8081 })
/* eslint-enable */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  const pReducer = persistReducer(persistConfig, rootReducers)
  if (__DEV__) {
    const store = createStore(
      // rootReducers,
      pReducer,
      initialState,
      composeEnhancers(
        applyMiddleware(
          sagaMiddleware,
          createLogger()
        )
      )
    )
    sagaMiddleware.run(rootSaga)
    return store
  }
  const store = createStore(
    // rootReducers,
    pReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(
        sagaMiddleware,
        // createLogger()
      )
    )
  )
  sagaMiddleware.run(rootSaga)
  return store
}

const store = configureStore()
const persistor = persistStore(store)

// export default store
export {
  store,
  persistor
}
