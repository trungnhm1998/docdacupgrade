import { Themes } from '../ui'

const DeliveryType = {
  hold: 0,
  pickup: 1,
  delivery: 2
}

const TicketType = {
  hold: 'Vé điện tử',
  pickup: 'Nhận tại đại lý',
  delivery: 'Giao tận nơi'
}

const CurrencyType = {
  10000: '10N',
  20000: '20N',
  50000: '50N',
  100000: '100N',
  200000: '200N',
  500000: '500N',
  1000000: '1T'
}

const withdrawIcon = {
  5: Themes.Images.momoIcon,
  8: Themes.Images.icon8,
  14: Themes.Images.icon4,
  15: Themes.Images.cardIcon,
  16: Themes.Images.cardIcon,
  17: Themes.Images.cardIcon,
  22: Themes.Images.icon8,
  23: Themes.Images.icon9,
  27: Themes.Images.icon10,
  100: Themes.Images.avtStoreVietllot,
  1: Themes.Images.icon1,
}

const PaymentGateway = {
  MOMO: '2',
  PAY_123: '4',
  INTERNET_BANKING: '8',
  ZALO_PAY: '9',
  CARD_TELCO: 'telco',
  CARD_GATE: '3',
  WEB_MONEY: '10',
  NAPAS: '1',
}

const VERSION_APP = '1.0.5'

export {
  DeliveryType,
  TicketType,
  CurrencyType,
  withdrawIcon,
  PaymentGateway,
  VERSION_APP
}
