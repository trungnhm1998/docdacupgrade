export const GET_INIT_LOTTERY_RESULT = 'GET_INIT_LOTTERY_RESULT'
export const GET_INIT_LOTTERY_RESULT_SUCCESS = 'GET_INIT_LOTTERY_RESULT_SUCCESS'
export const GET_INIT_LOTTERY_RESULT_FAIL = 'GET_INIT_LOTTERY_RESULT_FAIL'
export const GET_LOTTERY_645_RESULT = 'GET_LOTTERY_645_RESULT'
export const GET_LOTTERY_645_RESULT_SUCCESS = 'GET_LOTTERY_645_RESULT_SUCCESS'
export const GET_LOTTERY_655_RESULT = 'GET_LOTTERY_655_RESULT'
export const GET_LOTTERY_655_RESULT_SUCCESS = 'GET_LOTTERY_655_RESULT_SUCCESS'
export const GET_LOTTERY_MAX4D_RESULT = 'GET_LOTTERY_MAX4D_RESULT'
export const GET_LOTTERY_MAX4D_RESULT_SUCCESS = 'GET_LOTTERY_MAX4D_RESULT_SUCCESS'
export const GET_645_PRIZE_LIST = 'GET_645_PRIZE_LIST'
export const GET_645_PRIZE_LIST_SUCCESS = 'GET_645_PRIZE_LIST_SUCCESS'
export const GET_MAX4D_PRIZE_LIST = 'GET_MAX4D_PRIZE_LIST'
export const GET_MAX4D_PRIZE_LIST_SUCCESS = 'GET_MAX4D_PRIZE_LIST_SUCCESS'
export const CLEAR_LOTTERY_RESULT = 'CLEAR_LOTTERY_RESULT'
export const GET_TICKET_TYPE_LIST = 'GET_TICKET_TYPE_LIST'
export const GET_TICKET_TYPE_LIST_SUCCESS = 'GET_TICKET_TYPE_LIST_SUCCESS'
export const GET_BANK_INFO_SUCCESS = 'GET_BANK_INFO_SUCCESS'
export const GET_USER_LOTTERY = 'GET_USER_LOTTERY'
export const GET_USER_LOTTERY_SUCCESS = 'GET_USER_LOTTERY_SUCCESS'
export const GET_USER_LOTTERY_EMPTY = 'GET_USER_LOTTERY_EMPTY'
export const GET_USER_LOTTERY_DETAIL = 'GET_USER_LOTTERY_DETAIL'
export const GET_USER_LOTTERY_DETAIL_SUCCESS = 'GET_USER_LOTTERY_DETAIL_SUCCESS'
export const GET_LIST_DELIVERY_ADDRESS = 'GET_LIST_DELIVERY_ADDRESS'
export const GET_LIST_DELIVERY_ADDRESS_SUCCESS = 'GET_LIST_DELIVERY_ADDRESS_SUCCESS'
export const BUY_MEGA_TICKET = 'BUY_MEGA_TICKET'
export const BUY_MEGA_TICKET_SUCCESS = 'BUY_MEGA_TICKET_SUCCESS'
export const BUY_MEGA_POWER_TICKET = 'BUY_MEGA_POWER_TICKET'
export const BUY_MEGA_POWER_TICKET_SUCCESS = 'BUY_MEGA_POWER_TICKET_SUCCESS'
export const BUY_TICKET_PENDING = 'BUY_TICKET_PENDING'
export const BUY_MEGA_TICKET_NOT_ENOUGH_MONEY = 'BUY_MEGA_TICKET_NOT_ENOUGH_MONEY'
export const BUY_MAX_TICKET = 'BUY_MAX_TICKET'
export const BUY_MAX_TICKET_SUCCESS = 'BUY_MAX_TICKET_SUCCESS'
export const USER_CONFIRM_RECEIVE_TICKET = 'USER_CONFIRM_RECEIVE_TICKET'
export const USER_CONFIRM_RECEIVE_TICKET_SUCCESS = 'USER_CONFIRM_RECEIVE_TICKET_SUCCESS'
export const GET_LIST_PERIOD_TICKET = 'GET_LIST_PERIOD_TICKET'
export const GET_LIST_PERIOD_TICKET_SUCCESS = 'GET_LIST_PERIOD_TICKET_SUCCESS'
export const GET_TICKET_BY_PREIOD = 'GET_TICKET_BY_PREIOD'
export const GET_TICKET_BY_PREIOD_SUCCESS = 'GET_TICKET_BY_PREIOD_SUCCESS'
export const GET_TICKET_BY_PREIOD_EMPTY = 'GET_TICKET_BY_PREIOD_EMPTY'
export const GET_AGENT_LIST = 'GET_AGENT_LIST'
export const GET_AGENT_LIST_SUCCESS = 'GET_AGENT_LIST_SUCCESS'
export const GET_AGENT_REWARD_LIST = 'GET_AGENT_REWARD_LIST'
export const GET_AGENT_REWARD_LIST_SUCCESS = 'GET_AGENT_REWARD_LIST_SUCCESS'
export const GET_AGENT_LIST_EMPTY = 'GET_AGENT_LIST_EMPTY'
export const POST_TICKET_SCHEDULE_REWARD = 'POST_TICKET_SCHEDULE_REWARD'
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS'
export const GO_TO_RESULT_TAB = 'GO_TO_RESULT_TAB'
export const GET_BANNER_LIST = 'GET_BANNER_LIST'
export const GET_BANNER_LIST_SUCCESS = 'GET_BANNER_LIST_SUCCESS'

