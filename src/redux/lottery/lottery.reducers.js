import * as types from './lottery.types'
const initialState = {
  megaLotteryResult: [],
  max4dLotteryResult: [],
  deliveryLocation: []
}

export default function api(state = initialState, action) {
  switch (action.type) {
    case types.GET_LOTTERY_645_RESULT_SUCCESS: {
      const { data, page } = action.payload
      if (page === 1) {
        return {
          ...state,
          megaLotteryResult: [...data]
        }
      }
      return {
        ...state,
        megaLotteryResult: [...state.megaLotteryResult, ...data]
      }
    }
    case types.GET_LOTTERY_655_RESULT_SUCCESS: {
      const { data, page } = action.payload
      if (page === 1) {
        return {
          ...state,
          megaPowerLotteryResult: [...data]
        }
      }
      return {
        ...state,
        megaPowerLotteryResult: [...state.megaPowerLotteryResult, ...data]
      }
    }
    case types.GET_LOTTERY_MAX4D_RESULT_SUCCESS: {
      const { data, page } = action.payload
      if (page === 1) {
        return {
          ...state,
          max4dLotteryResult: [...data]
        }
      }
      return {
        ...state,
        max4dLotteryResult: [...state.max4dLotteryResult, ...data]
      }
    }
    case types.GET_645_PRIZE_LIST_SUCCESS:
      return {
        ...state,
        megaPrizeList: { ...action.payload }
      }
    case types.GET_MAX4D_PRIZE_LIST_SUCCESS:
      return {
        ...state,
        max4DPrizeList: { ...action.payload }
      }
    case types.CLEAR_LOTTERY_RESULT:
      return {}
    case types.GET_TICKET_TYPE_LIST_SUCCESS:
      return {
        ...state,
        ticketPrizeInfo: { ...action.payload }
      }
    case types.GET_USER_LOTTERY_SUCCESS: {
      const { data, page } = action.payload
      if (page === 1) {
        return {
          ...state,
          lotteryBuyList: [...data]
        }
      }
      return {
        ...state,
        lotteryBuyList: [...state.lotteryBuyList, ...data]
      }
    }
    case types.GET_USER_LOTTERY_DETAIL_SUCCESS:
      return {
        ...state,
        LotteryDetail: { ...action.payload }
      }
    case types.GET_LIST_DELIVERY_ADDRESS_SUCCESS: {
      return {
        ...state,
        deliveryLocation: { ...action.payload.data }
      }
    }
    case types.GET_USER_LOTTERY_EMPTY: {
      return {
        ...state,
        lotteryBuyList: []
      }
    }
    case types.GET_LIST_PERIOD_TICKET_SUCCESS: {
      return {
        ...state,
        listPeriodTicket: { ...action.payload.data }
      }
    }
    case types.GET_TICKET_BY_PREIOD_SUCCESS: {
      const { data, page } = action.payload
      if (page === 1) {
        return {
          ...state,
          lotteryBuyList: [...data]
        }
      }
      return {
        ...state,
        lotteryBuyList: [...state.lotteryBuyList, ...data]
      }
      // return {
      //   ...state,
      //   lotteryBuyList: { ...action.payload.data }
      // }
    }
    case types.GET_TICKET_BY_PREIOD_EMPTY: {
      return {
        ...state,
        lotteryBuyList: []
      }
    }
    case types.GET_AGENT_LIST_SUCCESS: {
      return {
        ...state,
        agentList: { ...action.payload.data }
      }
    }
    case types.GET_AGENT_REWARD_LIST_SUCCESS: {
      return {
        ...state,
        agentReward: { ...action.payload }
      }
    }
    case types.GET_AGENT_LIST_EMPTY: {
      return {
        ...state,
        agentList: null
      }
    }
    case types.LOGOUT_USER_SUCCESS: {
      return { ...state, listPeriodTicket: [], deliveryLocation: [] }
    }
    case types.GO_TO_RESULT_TAB: {
      return {
        ...state,
        initialPage: action.payload
      }
    }
    case types.GET_BANNER_LIST_SUCCESS: {
      return {
        ...state,
        bannerList: action.payload.data
      }
    }
    default:
      return state
  }
}

