import * as types from './device.types'

export const registerClient = (data) => {
  return {
    type: types.REGISTER_CLIENT,
    payload: data,
  }
}

export const registerPassLock = (passLock) => {
  return {
    type: types.REGISTER_PASS_LOCK,
    payload: {
      passLock,
    },
  }
}

export const removePassLock = () => {
  return {
    type: types.REMOVE_PASS_LOCK,
  }
}

export const setRoute = (routeName) => {
  return {
    type: types.SET_ROUTING,
    payload: {
      routeName
    }
  }
}

export const upgradeVersion = (version) => {
  return {
    type: types.UPGRADE_VERSION,
    payload: { version }
  }
}
