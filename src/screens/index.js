import React from 'react'
import { Navigation } from 'react-native-navigation'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import * as Components from '../components'

const ReduxComponent = (store, persistor, Component) => {
  return function inject(props) {
    const EnhancedComponent = () => (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Component
            {...props}
          />
        </PersistGate>
      </Provider>
    )

    return <EnhancedComponent />
  }
}

export const registerScreen = (store, persistor) => {
  //console.log('start registerScreen')
  Navigation.registerComponent('TestApp', () => Components.App)
  Navigation.registerComponent('Root', () => ReduxComponent(store, persistor, Components.Root))
  Navigation.registerComponent('SendOTPRegister', () => ReduxComponent(store, persistor, Components.SendOTPRegister))
  Navigation.registerComponent('Register', () => ReduxComponent(store, persistor, Components.Register))
  Navigation.registerComponent('RegisterAccountKit', () => ReduxComponent(store, persistor, Components.RegisterAccountKit))
  Navigation.registerComponent('Login', () => ReduxComponent(store, persistor, Components.Login))
  Navigation.registerComponent('Welcome', () => ReduxComponent(store, persistor, Components.Welcome))
  Navigation.registerComponent('Home', () => ReduxComponent(store, persistor, Components.Home))
  Navigation.registerComponent('Profile', () => ReduxComponent(store, persistor, Components.Profile))
  Navigation.registerComponent('ForgotPassword', () => ReduxComponent(store, persistor, Components.ForgotPassword))
  Navigation.registerComponent('ForgotPasswordAccountKit', () => ReduxComponent(store, persistor, Components.ForgotPasswordAccountKit))
  Navigation.registerComponent('PickupTicket', () => ReduxComponent(store, persistor, Components.PickupTicket))
  Navigation.registerComponent('PickupTicketMegaPower', () => ReduxComponent(store, persistor, Components.PickupTicketMegaPower))
  Navigation.registerComponent('PickupTicketMax', () => ReduxComponent(store, persistor, Components.PickupTicketMax))
  Navigation.registerComponent('PaymentHome', () => ReduxComponent(store, persistor, Components.PaymentHome))
  Navigation.registerComponent('DepositGatewayInput', () => ReduxComponent(store, persistor, Components.DepositGatewayInput))
  Navigation.registerComponent('DepositGatewayNapasInput', () => ReduxComponent(store, persistor, Components.DepositGatewayNapasInput))
  Navigation.registerComponent('DepositGatewayCard', () => ReduxComponent(store, persistor, Components.DepositGatewayCard))
  Navigation.registerComponent('DepositGatewaySuccess', () => ReduxComponent(store, persistor, Components.DepositGatewaySuccess))
  Navigation.registerComponent('DepositGatewayFail', () => ReduxComponent(store, persistor, Components.DepositGatewayFail))
  Navigation.registerComponent('DepositGatewayCheckout', () => ReduxComponent(store, persistor, Components.DepositGatewayCheckout))
  Navigation.registerComponent('DepositGatewayWebmoneyCheckout', () => ReduxComponent(store, persistor, Components.DepositGatewayWebmoneyCheckout))
  Navigation.registerComponent('WithdrawGateway', () => ReduxComponent(store, persistor, Components.WithdrawGateway))
  Navigation.registerComponent('WithdrawGatewayConfirm', () => ReduxComponent(store, persistor, Components.WithdrawGatewayConfirm))
  Navigation.registerComponent('PaymentHistory', () => ReduxComponent(store, persistor, Components.PaymentHistory))
  Navigation.registerComponent('SelectContacts', () => ReduxComponent(store, persistor, Components.SelectContacts))
  Navigation.registerComponent('SettingHome', () => ReduxComponent(store, persistor, Components.SettingHome))
  Navigation.registerComponent('SettingHomeIntro', () => ReduxComponent(store, persistor, Components.SettingHomeIntro))
  Navigation.registerComponent('SettingInfo', () => ReduxComponent(store, persistor, Components.SettingInfo))
  Navigation.registerComponent('LotteryResult', () => ReduxComponent(store, persistor, Components.LotteryResult))
  Navigation.registerComponent('Loading', () => ReduxComponent(store, persistor, Components.Loading))
  Navigation.registerComponent('LightBox', () => ReduxComponent(store, persistor, Components.LightBox))
  Navigation.registerComponent('LotteryHome', () => ReduxComponent(store, persistor, Components.LotteryHome))
  Navigation.registerComponent('Lottery645Delivery', () => ReduxComponent(store, persistor, Components.Lottery645Delivery))
  Navigation.registerComponent('Lottery645Pick', () => ReduxComponent(store, persistor, Components.Lottery645Pick))
  Navigation.registerComponent('Lottery645Stored', () => ReduxComponent(store, persistor, Components.Lottery645Stored))
  Navigation.registerComponent('Lottery645WinForm', () => ReduxComponent(store, persistor, Components.Lottery645WinForm))
  Navigation.registerComponent('Lottery655Delivery', () => ReduxComponent(store, persistor, Components.Lottery655Delivery))
  Navigation.registerComponent('Lottery655Pick', () => ReduxComponent(store, persistor, Components.Lottery655Pick))
  Navigation.registerComponent('Lottery655Stored', () => ReduxComponent(store, persistor, Components.Lottery655Stored))
  Navigation.registerComponent('Lottery655WinForm', () => ReduxComponent(store, persistor, Components.Lottery655WinForm))
  Navigation.registerComponent('LotteryMax4dDelivery', () => ReduxComponent(store, persistor, Components.LotteryMax4dDelivery))
  Navigation.registerComponent('LotteryMax4dPick', () => ReduxComponent(store, persistor, Components.LotteryMax4dPick))
  Navigation.registerComponent('LotteryMax4dStored', () => ReduxComponent(store, persistor, Components.LotteryMax4dStored))
  Navigation.registerComponent('NewAddress', () => ReduxComponent(store, persistor, Components.AddAddress))
  Navigation.registerComponent('AddressList', () => ReduxComponent(store, persistor, Components.AddressList))
  Navigation.registerComponent('DepositBankingInput', () => ReduxComponent(store, persistor, Components.DepositBankingInput))
  Navigation.registerComponent('DetailMax4d', () => ReduxComponent(store, persistor, Components.DetailMax4d))
  Navigation.registerComponent('ConfirmMax4d', () => ReduxComponent(store, persistor, Components.ConfirmMax4d))
  Navigation.registerComponent('ConfirmMega', () => ReduxComponent(store, persistor, Components.ConfirmMega))
  Navigation.registerComponent('ConfirmMegaPower', () => ReduxComponent(store, persistor, Components.ConfirmMegaPower))
  Navigation.registerComponent('DetailMega', () => ReduxComponent(store, persistor, Components.DetailMega))
}
