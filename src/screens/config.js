import {
  Themes
} from '../ui'

const Config = {
  screen: {
    sendOTPRegister: {
      name: 'SendOTPRegister',
      options: {
        topBar: {
          title: {
            text: ''
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    home: {
      name: 'Home',
      options: {
        topBar: {
          title: {
            text: ''
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    login: {
      name: 'Login',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            text: 'Đăng nhập',
            color: '#fff'
          },
          backButton: {
            enable: true,
            title: '',
            showTitle: true,
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    welcome: {
      name: 'Welcome',
      options: {
        topBar: {
          title: {
            text: 'Đăng nhập'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    profile: {
      name: 'Profile',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Hồ sơ cá nhân'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        },
        animations: {
          push: {
            waitForRender: true
          },
          showModal: {
            waitForRender: true
          }
        }
      }
    },
    forgotPassword: {
      name: 'ForgotPassword',
      options: {
        topBar: {
          title: {
            text: 'Quên mật khẩu'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    forgotPasswordAccountKit: {
      name: 'ForgotPasswordAccountKit',
      options: {
        topBar: {
          title: {
            text: 'Quên mật khẩu'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    register: {
      name: 'Register',
      options: {
        topBar: {
          title: {
            text: 'Kích hoạt'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    registerAccountKit: {
      name: 'RegisterAccountKit',
      options: {
        topBar: {
          title: {
            text: 'Kích hoạt'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    megaPickup: {
      name: 'PickupTicket',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chọn cách chơi'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: '#fff'
        }
      }
    },
    megaPowerPickup: {
      name: 'PickupTicketMegaPower',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chọn cách chơi'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: '#fff'
        }
      }
    },
    megaPickupMax: {
      name: 'PickupTicketMax',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chọn cách chơi'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: '#fff'
        }
      }
    },
    depositGatewayInput: {
      name: 'DepositGatewayInput',
      options: {
        topBar: {
          title: {
            text: 'Nạp tiền tài khoản'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewayNapasInput: {
      name: 'DepositGatewayNapasInput',
      options: {
        topBar: {
          title: {
            text: 'Nạp tiền qua Napas'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewayCard: {
      name: 'DepositGatewayCard',
      options: {
        topBar: {
          title: {
            text: 'Nạp tiền tài khoản'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewaySuccess: {
      name: 'DepositGatewaySuccess',
      options: {
        topBar: {
          title: {
            text: 'Giao dịch thành công'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewayFail: {
      name: 'DepositGatewayFail',
      options: {
        topBar: {
          title: {
            text: 'Giao dịch thất bại'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewayCheckout: {
      name: 'DepositGatewayCheckout',
      options: {
        topBar: {
          title: {
            text: 'Thanh toán'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositGatewayWebmoneyCheckout: {
      name: 'DepositGatewayWebmoneyCheckout',
      options: {
        topBar: {
          title: {
            text: 'Thanh toán'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    paymentHome: {
      name: 'PaymentHome',
      options: {
        topBar: {
          title: {
            text: 'Tài khoản'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    paymentHistory: {
      name: 'PaymentHistory',
      options: {
        topBar: {
          title: {
            text: 'Lịch sử giao dịch'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    selectContacts: {
      name: 'SelectContacts',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chọn SĐT'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    settingHome: {
      name: 'SettingHome',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Cài đặt'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        },
        animations: {
          push: {
            waitForRender: true
          }
        }
      }
    },
    settingHomeIntro: {
      name: 'SettingHomeIntro',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Cài đặt'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    settingInfo: {
      name: 'SettingInfo',
      options: {
        topBar: {
          title: {
            text: ''
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery645Delivery: {
      name: 'Lottery645Delivery',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery645Pick: {
      name: 'Lottery645Pick',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery645Stored: {
      name: 'Lottery645Stored',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery645WinForm: {
      name: 'Lottery645WinForm',
      options: {
        topBar: {
          title: {
            text: 'Đăng ký nhận thưởng'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery655WinForm: {
      name: 'Lottery655WinForm',
      options: {
        topBar: {
          title: {
            text: 'Đăng ký nhận thưởng'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery655Delivery: {
      name: 'Lottery655Delivery',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery655Pick: {
      name: 'Lottery655Pick',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lottery655Stored: {
      name: 'Lottery655Stored',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lotteryMax4dDelivery: {
      name: 'LotteryMax4dDelivery',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lotteryMax4dPick: {
      name: 'LotteryMax4dPick',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lotteryMax4dStored: {
      name: 'LotteryMax4dStored',
      options: {
        topBar: {
          title: {
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    depositBankingInput: {
      name: 'DepositBankingInput',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chuyển khoản ngân hàng'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        },
        animations: {
          push: {
            waitForRender: true
          },
          showModal: {
            waitForRender: true
          }
        }
      }
    },
    withdrawGateway: {
      name: 'WithdrawGateway',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Rút tiền về tài khoản'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    withdrawGatewayConfirm: {
      name: 'WithdrawGatewayConfirm',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Xác nhận rút tiền'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    lotteryResult: {
      name: 'LotteryResult',
      options: {
        topBar: {
          title: {
            text: 'Kết quả xổ số'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    detailMax4d: {
      name: 'DetailMax4d',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chi tiết thanh toán'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    confirmMax4d: {
      name: 'ConfirmMax4d',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Xác nhận mua vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    confirmMega: {
      name: 'ConfirmMega',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Xác nhận mua vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    confirmMegaPower: {
      name: 'ConfirmMegaPower',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Xác nhận mua vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    detailMega: {
      name: 'DetailMega',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chi tiết thanh toán'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    detailLottery645Delivery: {
      name: 'Lottery645Delivery',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    detailLottery645Pick: {
      name: 'Lottery645Pick',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    },
    detailLottery645Stored: {
      name: 'Lottery645Stored',
      options: {
        topBar: {
          visible: true,
          drawBehind: false,
          noBorder: true,
          background: {
            color: Themes.Colors.navColor
          },
          title: {
            color: '#fff',
            text: 'Chi tiết vé'
          },
          backButton: {
            title: '',
            color: '#fff'
          }
        },
        layout: {
          backgroundColor: 'transparent'
        }
      }
    }
  },
  navigatorStyle: {
    // DEFAULT_NAV_STYLE: {
    //   navBarBackgroundColor: Themes.Colors.navColor,
    //   navBarNoBorder: true,
    //   statusBarTextColorSchemeSingleScreen: 'light',
    //   statusBarTextColorScheme: 'light',
    //   navBarButtonColor: '#fff',
    //   navBarTextColor: '#fff'
    // },
    DEFAULT_NAV_STYLE: {
      topBar: {
        visible: true,
        drawBehind: false,
        noBorder: true,
        background: {
          color: Themes.Colors.navColor
        },
        title: {
          color: '#fff'
        },
        // rightButtons: [
        //   {
        //     color: '#fff'
        //   }
        // ],
        // leftButtons: [
        //   {
        //     color: '#fff'
        //   }
        // ]
        backButton: {
          color: '#fff',
          title: ''
        }
      },
      bottomTab: {
        selectedIconColor: Themes.Colors.darkRed,
        selectedTextColor: Themes.Colors.darkRed,
        textColor: '#bdbdbd',
        iconColor: '#bdbdbd'
      },
      statusBar: {
        style: 'light',
        backgroundColor: Themes.Colors.navColor
      }
    },
    DEFAULT_NAV_STYLE_HIDDEN_TAB: {
      navBarBackgroundColor: Themes.Colors.navColor,
      navBarNoBorder: true,
      statusBarTextColorSchemeSingleScreen: 'light',
      statusBarTextColorScheme: 'light',
      navBarButtonColor: '#fff',
      navBarTextColor: '#fff',
      tabBarHidden: true,
      statusBarColor: Themes.Colors.navColor
    },
    DEFAULT_NAV_STYLE_CUSTOM: {
      statusBarTextColorSchemeSingleScreen: 'light',
      statusBarTextColorScheme: 'light',
      navBarHidden: true,
      navBarTranslucent: true,
      tabBarHidden: true
    }
  }
}
export default Config
