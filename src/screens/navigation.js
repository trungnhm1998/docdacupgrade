import { Navigation as Navigator } from 'react-native-navigation'
import { Platform } from 'react-native'
import { store } from '../configs/store.config'
import { VERSION } from '../configs/version.config'
import { Icon, Themes } from '../ui'

const Navigation = {
  navToTestApp: () => {
    Navigator.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'TestApp'
            }
          }]
        }
      }
    })
  },
  navToRoot: () => {
    Navigator.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'Root'
            }
          }],
          options: {
            layout: {
              orientation: 'portrait'
            },
            topBar: {
              visible: false,
              drawBehind: true
            },
            statusBar: {
              backgroundColor: Themes.Colors.navColor
            }
          }
        }
      }
    })
  },
  navToLogin: () => {
    Navigator.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'SendOTPRegister'
            }
          }],
          options: {
            layout: {
              orientation: 'portrait'
            },
            topBar: {
              visible: false,
              drawBehind: true
            }
          }
        }
      }
    })
  },
  navToWelcome: () => {
    Navigator.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'Welcome'
            }
          }],
          options: {
            topBar: {
              title: {
                text: 'Đăng nhập'
              },
              visible: true,
              drawBehind: false,
              noBorder: true,
              background: {
                color: Themes.Colors.navColor
              }
            },
            layout: {
              orientation: 'portrait'
            }
          }
        }
      }
    })
  },
  navToHome: () => {
    const { accountInfo } = store.getState()
    Promise.all([
      Icon.getImageSource('nav-cup', 20, '#bdbdbd'),
      Icon.getImageSource('nav-club', 20, '#bdbdbd'),
      Icon.getImageSource('nav-ball', 20, '#bdbdbd'),
      Icon.getImageSource('nav-note', 18, '#bdbdbd'),
      Icon.getImageSource('nav-user', 20, '#bdbdbd')
    ]).then((values) => {
      const tabs = [{
        stack: {
          children: [{
            component: {
              name: 'Home',
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true
                },
                statusBar: {
                  backgroundColor: Themes.Colors.navColor
                },
                bottomTab: {
                  iconColor: '#bdbdbd',
                  textColor: '#bdbdbd',
                  selectedIconColor: Themes.Colors.darkRed,
                  selectedTextColor: Themes.Colors.darkRed,
                  text: 'Trò chơi',
                  icon: values[0]
                }
              }
            }
          }]
        }
      },
      {
        stack: {
          children: [{
            component: {
              name: 'LotteryResult',
              //id: 'LotteryResult', // khong co dong nay ko chuyen tu cac ky quay so sang kqsx dc, ma co thi no bi bug ben ios, ngu nguoi
              options: {
                bottomTab: {
                  iconColor: '#bdbdbd',
                  textColor: '#bdbdbd',
                  selectedIconColor: Themes.Colors.darkRed,
                  selectedTextColor: Themes.Colors.darkRed,
                  text: 'KQXS',
                  icon: values[2]
                }, 
                topBar: {
                  visible: true,
                  drawBehind: false,
                  noBorder: true,
                  animated: false,
                  background: {
                    color: Themes.Colors.navColor
                  },
                  title: {
                    color: '#fff',
                    text: 'Kết quả xổ số',
                  },
                  
                },
                statusBar: {
                  backgroundColor: Themes.Colors.navColor
                },
                
              }
            }
          }]
        }
      },
      {
        stack: {
          children: [{
            component: {
              name: 'LotteryHome',
              options: {
                bottomTab: {
                  iconColor: '#bdbdbd',
                  textColor: '#bdbdbd',
                  selectedIconColor: Themes.Colors.darkRed,
                  selectedTextColor: Themes.Colors.darkRed,
                  text: 'Lịch sử',
                  icon: values[3]
                },
                topBar: {
                  visible: true,
                  drawBehind: false,
                  noBorder: true,
                  animated: false,
                  background: {
                    color: Themes.Colors.navColor
                  },
                  title: {
                    color: '#fff',
                    text: 'Vé của bạn'
                  }
                }
              }
            }
          }]
        }
      }]
      if (accountInfo.production_version !== VERSION || Platform.OS === 'android') {
        tabs.push({
          stack: {
            children: [{
              component: {
                name: 'PaymentHome',
                options: {
                  bottomTab: {
                    iconColor: '#bdbdbd',
                    textColor: '#bdbdbd',
                    selectedIconColor: Themes.Colors.darkRed,
                    selectedTextColor: Themes.Colors.darkRed,
                    text: 'Tài khoản',
                    icon: values[4]
                  },
                  topBar: {
                    visible: true,
                    drawBehind: false,
                    noBorder: true,
                    animated: false,
                    background: {
                      color: Themes.Colors.navColor
                    },
                    title: {
                      color: '#fff',
                      text: 'Tài khoản'
                    },
                   
                    
                  }
                }
              }
            }]
          }
        })
      }
      Navigator.setRoot({
        root: {
          bottomTabs: {
            children: tabs,
            options: {
              bottomTabs: {
                // backgroundColor: '#bdbdbd',
                backgroundColor: '#fff',
                titleDisplayMode: 'alwaysShow'
              },
              layout: {
                orientation: 'portrait'
              }
            }
          }
        }
      })
    }).catch((error) => {
      console.error('error', error)
    })
  },
  showLoading: () => {
    // hasCallHideLoading = false
    // Navigator.showModal({
    //   component: {
    //     name: 'Loading',
    //     options: {
    //       layout: {
    //         backgroundColor: Platform.OS === 'ios'
    //           ? 'rgba(255, 255, 255, 0)'
    //           : 'rgba(0, 0, 0, 0.4)',
    //         componentBackgroundColor: 'rgba(0, 0, 0, 0.4)'
    //       },
    //       topBar: {
    //         visible: false,
    //         drawBehind: true,
    //         animate: false
    //       }
    //     }
    //   }
    // })

    Navigator.showOverlay({
      component: {
        name: 'Loading',
        options: {
          layout: {
            backgroundColor: Platform.OS === 'ios' ? 'rgba(0, 0, 0, 0.5)' : 'rgba(0, 0, 0, 0.4)',
            componentBackgroundColor: Platform.OS === 'ios' ? 'rgba(0, 0, 0, 0.5)' : 'rgba(0,  0, 0, 0.4)'
          },
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          }
        }
      }
    })
  },
  hideLoading: (componentId) => {
    if (componentId) {
      setTimeout(() => {
        Navigator.dismissOverlay(componentId)
      }, 300)
    }
  },
  showMessage: (message, buttons, options) => {
    setTimeout(() => {
      Navigator.showModal({
        stack: {
          children: [{
            component: {
              name: 'LightBox',
              options: {
                layout: {
                  backgroundColor: 'rgba(201, 32, 51, 0)'
                }
              },
              passProps: {
                message,
                buttons,
                options
              }
            }
          }]
        }
      })
    }, 400)
  },
  hideMessage: () => {
    Navigator.dismissAllModals()
  }
}

export {
  Navigation
}
