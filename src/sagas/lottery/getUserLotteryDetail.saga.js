import { takeLatest, call, put, select } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/lottery'
import { docdacAPI } from '../../api/'
const accessToken = state => state.accountInfo.accessToken

function* doAction(action) {
  const token = yield select(accessToken)
  const { ticketID } = action.payload
  try {
     const { code, msg, data } = yield call(docdacAPI.getUserTicketDetail, { ticket_id: ticketID }, token)
     switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        const dispatchAction = {
          type: types.GET_USER_LOTTERY_DETAIL_SUCCESS,
          payload: data
        }
        yield put(dispatchAction)
        yield put(apiActions.storeResponseMessage({ type: types.GET_USER_LOTTERY_DETAIL_SUCCESS }))
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_USER_LOTTERY_DETAIL, doAction)
}
