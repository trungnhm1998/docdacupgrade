import { delay } from 'redux-saga'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/init'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    const { code, data } = yield call(accountAPI.getInitConfig)

    switch (code) {
      case responseCode.GET_INIT_CONFIG_SUCCESS: {
        yield put({ type: types.GET_INIT_CONFIG_SUCCESS, payload: data })
        break
      }
      default: {
        yield put({ type: types.GET_INIT_CONFIG_FAIL })
      }
    }
  } catch (error) {
    yield put({ type: types.GET_INIT_CONFIG_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_INIT_CONFIG, doAction)
}
