import Authorize from './authorize.saga'
import SendOTPRegister from './sendOTPRegister.saga'
import ReSendOTPRegister from './reSendOTPRegister.saga'
import SendOTPForgotPassword from './sendOTPForgotPassword.saga'
import VerifyPhone from './verifyPhone.saga'
import Register from './register.saga'
import RegisterAccountKit from './registerAccountKit.saga'
import GetUserInfo from './getUserInfo.saga'
import GetUserInfoDocdac from './getUserInfoDocdac.saga'
import Logout from './logout.saga'
import UpdateUserInfo from './updateUserInfo.saga'
import ForgotPassword from './forgotPass.saga'
import ForgotPasswordAccountKit from './forgotPassAccountKit.saga'
import GetUserAddress from './getUserAddress.saga'
import UploadAvatar from './uploadAvatar.saga'
import AddNewAddress from './addNewAddress.saga'
import RechargeHistory from './rechargeHistory.saga'
import WithdrawHistory from './withdrawHistory.saga'
import MoneyHistory from './moneyHistory.saga'
import GetWithdrawHistory from './getWithdrawHistory.saga'
import SendOrderWithdraw from './sendOrderWithdraw.saga'
import GetMoneySetting from './getMoneySetting.saga'
import TransferMoney from './transferMoney.saga'
import GetTransferHistory from './getTransferHistory.saga'

export {
  Authorize,
  SendOTPRegister,
  ReSendOTPRegister,
  SendOTPForgotPassword,
  VerifyPhone,
  Register,
  RegisterAccountKit,
  GetUserInfo,
  Logout,
  UpdateUserInfo,
  ForgotPassword,
  ForgotPasswordAccountKit,
  GetUserInfoDocdac,
  GetUserAddress,
  UploadAvatar,
  AddNewAddress,
  RechargeHistory,
  WithdrawHistory,
  MoneyHistory,
  GetWithdrawHistory,
  SendOrderWithdraw,
  GetMoneySetting,
  TransferMoney,
  GetTransferHistory
}
