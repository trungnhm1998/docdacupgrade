import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api/index'
import { types } from '../../redux/account/'
import { docdacAPI } from '../../api/index'

function* doAction(action) {
  try {
    const { accessToken } = action.payload
    yield put(apiActions.clearResponse())
    const { code, data, msg } = yield call(docdacAPI.userAddressList, accessToken)
    switch (code) {
      case responseCode.REQUEST_SUCCESS:
      case responseCode.DATA_EMPTY: {
        const dispatchAction = {
          type: types.GET_USER_ADDRESS_SUCCESS,
          payload: data || []
        }
        yield put(dispatchAction)
        yield put(apiActions.storeResponseMessage(dispatchAction))
        break
      }
      default: {
        break
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_USER_ADDRESS, doAction)
}
