import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { avatar, callback } = action.payload
    const { code, data, message } = yield call(accountAPI.uploadAvatar, { avatar })
    switch (code) {
      case responseCode.UPLOAD_AVATAR_SUCCESS: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put({ type: types.UPLOAD_AVATAR_SUCCESS, payload: { avatarUrl: data.avatar_url } })
        callback(data.avatar_url)
        break
      }
      default: {
        yield put({ type: apiTypes.HIDE_LOADING })
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.UPLOAD_AVATAR, doAction)
}
