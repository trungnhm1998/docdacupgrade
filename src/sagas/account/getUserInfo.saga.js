import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { accountAPI } from '../../api/'

function* doAction(action) {
  try {
    const { accessToken } = action.payload
    const { code, data, message } = yield call(accountAPI.getInfo, { accessToken })
    
    switch (code) {
      case responseCode.GET_INFO_SUCCESS: {
        const dispatchAction = {
          type: types.GET_USERINFO_SUCCESS,
          payload: {
            account: data.account,
            accountId: data.account_id,
            address: data.address,
            avatarUrl: data.avatar_url,
            birthday: data.birthday,
            email: data.email,
            gender: data.gender,
            idNumber: data.id_number,
            name: data.name,
            isProfileUpdated: data.profile_update
          }
        }

        yield put(dispatchAction)
        yield put(apiActions.storeResponseMessage(dispatchAction))
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: apiTypes.STORE_API_MESSAGE, message, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_USERINFO, doAction)
}
