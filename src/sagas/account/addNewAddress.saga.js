import { delay } from 'redux-saga'
import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data, msg } = yield call(docdacAPI.userAddressAdd, { data: action.payload.data, accessToken: action.payload.accessToken })
    yield put({ type: apiTypes.HIDE_LOADING })

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        const { receiver_name, receiver_phone, district_id, province_id, ward_id, province, district_name, ward_name, address } = action.payload.data
        yield put({
          type: types.ADD_NEW_ADDRESS_SUCCESS,
          payload: {
            client_name: receiver_name,
            client_phone: receiver_phone,
            address_id: data.address_id,
            address,
            province_id,
            province_name: province,
            district_id,
            district_name,
            ward_id,
            ward_name
          }
        })
        yield put(apiActions.storeResponseMessage({ type: types.ADD_NEW_ADDRESS_SUCCESS, message: msg }))
        break
      }
      default:
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.ADD_NEW_ADDRESS, doAction)
}
