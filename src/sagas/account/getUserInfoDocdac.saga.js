import { takeLatest, call, put } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCodeDocdac.config'
import { types as apiTypes, actions as apiActions } from '../../redux/api'
import { types } from '../../redux/account'
import { docdacAPI } from '../../api/'

function* doAction(action) {
  try {
    const { accessToken, os, env, deviceToken, navigator } = action.payload
    const { code, data, msg } = yield call(docdacAPI.getUserInfo, { os, env, device_token: deviceToken }, accessToken)

    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put(apiActions.storeResponseMessage({ type: types.GET_USERINFO_DOCDAC_SUCCESS }))
        yield put({
          type: types.GET_USERINFO_DOCDAC_SUCCESS,
          payload: { ...data }
        })
        if (navigator) {
          navigator()
        }
        break
      }
      default: {
        yield put(apiActions.storeResponseMessage({ type: types.GET_USERINFO_DOCDAC_FAIL, code, message: msg, isDefault: true }))
        break
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_USERINFO_DOCDAC, doAction)
}
