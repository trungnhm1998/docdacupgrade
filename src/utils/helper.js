const pad = (padString, str, padLeft) => {
  if (typeof str === 'undefined') {
    return padString
  }
  if (padLeft) {
    return (padString + str).slice(-padString.length)
  } 
  return (str + padString).substring(0, padString.length)
}

const calculateCombination = (play) => {
  const combination = {
    5: 40,
    7: 7,
    8: 28,
    9: 84,
    10: 210,
    11: 462,
    12: 924,
    13: 1716,
    14: 3003,
    15: 5005,
    18: 18564
  }
  return combination[play]
}

const calculateCombinationPower6 = (play) => {
  const combination = {
    5: 50,
    7: 7,
    8: 28,
    9: 84,
    10: 210,
    11: 462,
    12: 924,
    13: 1716,
    14: 3003,
    15: 5005,
    18: 18564
  }
  return combination[play]
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href
  name = name.replace(/[\[\]]/g, '\\$&')
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`)

    
  const results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

const combineRNNV2Style = (style1, style2) => {
  return {
    topBar: {
      ...style1.topBar,
      ...style2.topBar,
      title: {
        ...style1.topBar.title,
        ...style2.topBar.title
      },
      backButton: {
        ...style1.topBar.backButton,
        ...style2.topBar.backButton
      }
    },
    statusBar: {
      ...style1.statusBar,
      ...style2.statusBar
    },
    layout: {
      ...style1.layout,
      ...style2.layout
    }
  }
}

export {
  calculateCombination,
  pad,
  getParameterByName,
  calculateCombinationPower6,
  combineRNNV2Style
}
