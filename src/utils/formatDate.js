import moment from 'moment'

const dateFormatList = {
  CLIENT_DATE_TIME: 'HH:mm DD/MM/YYYY',
  CLIENT_DATE: 'DD/MM/YYYY',
  SERVER_DATE_TIME: 'YYYY-MM-DD HH:mm:ss',
  SERVER_DATE: 'YYYY-MM-DD'
}

const formatDate = (value) => {
  if (!value) return ''
  const date = value.split('/')
  return `${date[2]}-${date[1]}-${date[0]}`
}

const formatServerTimeToShow = (time) => {
  if (!time) return ''
  return moment(time, dateFormatList.SERVER_DATE_TIME).format(dateFormatList.CLIENT_DATE_TIME)
}

const formatServerDateToShow = (time) => {
  if (!time) return ''
  return moment(time, dateFormatList.SERVER_DATE).format(dateFormatList.CLIENT_DATE)
}

export {
  formatDate,
  formatServerTimeToShow,
  dateFormatList
}
