import React, { Component } from 'react'
import { Platform, StatusBar, View } from 'react-native'
import { Colors } from '../../ui/themes'

class StatusBarAndroid extends Component {
  render() {
    const { barStyle } = this.props
    return (
      <View style={{ width: '100%' }}>
        <StatusBar barStyle={barStyle || 'light-content'} backgroundColor={Colors.navColor} translucent={false} />
      </View>
    )
  }
}

export default StatusBarAndroid
