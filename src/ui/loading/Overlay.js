import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'

class Loading extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false
    }
    this.onPressOverlay = this.onPressOverlay.bind(this)
  }

  componentWillReceiveProps(props) {
    this.setState({ visible: props.visible })
  }

  onPressOverlay() {
    this.setState({ visible: false })
    this.props.onPressOverlay()
  }

  render() {
    const { visible } = this.state
    if (visible) {
      return (
        <TouchableOpacity
          onPress={this.onPressOverlay} style={{ position: 'absolute', top: 0, right: 0, width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.5)' }}
        />
      )
    }
    return null
  }
}

export default Loading
