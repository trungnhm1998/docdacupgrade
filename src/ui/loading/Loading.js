import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'

class Loading extends Component {
  render() {
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center',flex:1,backgroundColor: '#fff' }}>
        <ActivityIndicator
          animating
          style={{ height: 80 }}
        />
      </View>
    )
  }
}

export default Loading
