import { createIconSetFromFontello } from 'react-native-vector-icons'
import fontelloConfig from '../../configs/icon.config.json'

const Icon = createIconSetFromFontello(fontelloConfig)
export default Icon
