import { TextInput } from 'react-native'

export function hideKeyboard() {
  TextInput.State.blurTextInput(TextInput.State.currentlyFocusedField())
}
