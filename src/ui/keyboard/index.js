import { hideKeyboard } from './KeyboardControl'
import KeyboardSpacer from './KeyboardSpacer'

export {
  hideKeyboard,
  KeyboardSpacer,
}
