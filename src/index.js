import React from 'react'
import { Alert, View, Platform } from 'react-native'
import { Provider } from 'react-redux'
import _ from 'lodash'
import { Navigation } from 'react-native-navigation'
import { store, persistor } from './configs/store.config'
import { registerScreen } from './screens'
import { Navigation as Navigator } from './screens/navigation'
import { Themes } from './ui'
import Config from './screens/config'
// registerScreen(store, Provider)
registerScreen(store, persistor)

class App extends React.Component {
  constructor(props) {
    super(props)
    //console.log('navTo')
    // store.subscribe(this.onStoreUpdate.bind(this))
    store.subscribe(() => this.onStoreUpdate())
    if (Platform.OS === 'android') {
      Navigation.setDefaultOptions(Config.navigatorStyle.DEFAULT_NAV_STYLE)
    }
    Navigator.navToRoot()
  }

  onStoreUpdate() {
    const { apiResponse } = store.getState()
    console.log('storeUpdate', store.getState())
    if (apiResponse.showLoading === true && apiResponse.loadingComponentId === '') {
      Navigator.showLoading()
      console.log('show loading here')
    } else if (apiResponse.showLoading === false && apiResponse.loadingComponentId !== '') {
      console.log('hide loading here', apiResponse)
      Navigator.hideLoading(apiResponse.loadingComponentId)
    }

    if (_.isEmpty(apiResponse.message) === false && apiResponse.isDefault === true) {
      if (_.isNumber(apiResponse.code) === true) {
        switch (apiResponse.code) {
          case -1000: {
            Alert.alert('Thông báo', apiResponse.message, [{
              text: 'Thử lại',
              onPress: () => {
                store.dispatch({ type: 'LOGOUT_USER_SUCCESS' })
                store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                Navigator.navToLogin()
              }
            }], { isLogout: true })
            break
          }
          case -7: {
            Alert.alert('Thông báo', apiResponse.message, [{
              text: 'Đăng nhập lại',
              onPress: () => {
                store.dispatch({ type: 'LOGOUT_USER_SUCCESS' })
                store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                Navigator.navToLogin()
              }
            }], { isLogout: true })
            break
          }
          default: {
            setTimeout(() => {
              Alert.alert('Thông báo', apiResponse.message, [{
                text: 'Đóng',
                onPress: () => {
                  store.dispatch({ type: 'CLEAR_API_RESPONSE' })
                }
              }])
            }, 200)
          }
        }
      }
    } else if (apiResponse.code === -1 && apiResponse.isDefault === true) {
      store.dispatch({ type: 'CLEAR_API_RESPONSE' })
      setTimeout(() => {
        Alert.alert('Thông báo', 'Vui lòng kiểm tra kết nối mạng.', [{
          text: 'Thử lại',
          onPress: () => {
            Navigator.navToRoot()
          }
        }])
      }, 200)
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

      </View>
    )
  }
}

export default App
