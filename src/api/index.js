import accountAPI from './docdac/accountAPI'
import docdacAPI from './docdac/docdacAPI'

export {
  accountAPI, docdacAPI
}
