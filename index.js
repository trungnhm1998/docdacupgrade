/** @format */
import { Navigation } from 'react-native-navigation'
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);

import App from './src'
import Config from './src/screens/config'

Navigation.registerComponent('entryApp', () => App)

Navigation.events().registerAppLaunchedListener(() => {
  // Navigation.setDefaultOptions(Config.navigatorStyle.DEFAULT_NAV_STYLE)
  Navigation.setRoot({
    root: {
      stack: {
        children: [{
          component: {
            name: 'entryApp'
          }
        }],
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          }
        }
      }
    }
  })
})
